const chai = require('chai');

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const { stubSpi, stubGpio } = require('../../../fixtures/kaluma.js');
const { counter, counterReset } = require('../../../fixtures/next_call.js');

const { driverOutputControl } = require('./driver_output_control.js');

chai.use(sinonChai);
const { expect } = chai;

describe('driver_output_control', () => {
  const connection = {
    csPin: stubGpio(1, OUTPUT),
    dcPin: stubGpio(1, OUTPUT),
    resetPin: stubGpio(1, OUTPUT),
    busyPin: stubGpio(1, INPUT),
  };
  const spi = stubSpi();
  const tCount = counter();

  beforeEach('reset', () => {
    sinon.reset();
    counterReset();
  });

  describe('yResolution checks', () => {
    it('should throw error for zero yResolution', async () => {
      expect(() => driverOutputControl(connection, spi, { resolutionY: 0 })).to.throw();
    });

    it('should throw error for 1 yResolution', async () => {
      expect(() => driverOutputControl(connection, spi, { resolutionY: 1 })).to.throw();
    });

    it('should throw error for decimal yResolution', async () => {
      expect(() => driverOutputControl(connection, spi, { resolutionY: 1.03 })).to.throw();
    });

    it('should throw error for string yResolution', async () => {
      expect(() => driverOutputControl(connection, spi, { resolutionY: '1' })).to.throw();
    });

    it('should throw error for undefined yResolution', async () => {
      expect(() => driverOutputControl(connection, spi, { resolutionY: undefined })).to.throw();
    });
  });

  describe('call', () => {
    it('should succeed with display of 296', async () => {
      driverOutputControl(connection, spi, { resolutionY: 296 });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x01]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x27]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x01]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x00]));
    });

    it('should succeed with display of 200', async () => {
      driverOutputControl(connection, spi, { resolutionY: 200 });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x01]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0xC7]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x00]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x00]));
    });

    it('should succeed with display address of 296', async () => {
      driverOutputControl(connection, spi, { startY: 1, endY: 296 });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x01]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x27]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x01]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x00]));
    });

    it('should succeed with display of address of 200', async () => {
      driverOutputControl(connection, spi, { startY: 1, endY: 200 });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x01]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0xC7]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x00]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x00]));
    });
  });
});
