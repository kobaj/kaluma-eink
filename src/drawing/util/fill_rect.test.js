const chai = require('chai');

const { fillRect } = require('./fill_rect.js');

const { expect } = chai;

describe('fill_rect', () => {
  const tests = [
    // Single byte different widths
    { arg: { bytes: [0x00], resolutionX: 8, resolutionY: 1, positionX: 0, positionY: 0, width: 8, height: 1, value: 0xFF }, expected: [0xFF] },
    { arg: { bytes: [0x00], resolutionX: 8, resolutionY: 1, positionX: 0, positionY: 0, width: 18, height: 1, value: 0xFF }, expected: [0xFF] },
    { arg: { bytes: [0x00], resolutionX: 8, resolutionY: 1, positionX: 0, positionY: 0, width: 1, height: 1, value: 0xFF }, expected: [0b10000000] },
    { arg: { bytes: [0x00], resolutionX: 8, resolutionY: 1, positionX: 0, positionY: 0, width: 4, height: 1, value: 0xFF }, expected: [0xF0] },
    // Single byte different start position
    { arg: { bytes: [0x00], resolutionX: 8, resolutionY: 1, positionX: 1, positionY: 0, width: 8, height: 1, value: 0xFF }, expected: [0b01111111] },
    { arg: { bytes: [0x00], resolutionX: 8, resolutionY: 1, positionX: 4, positionY: 0, width: 18, height: 1, value: 0xFF }, expected: [0x0F] },
    { arg: { bytes: [0x00], resolutionX: 8, resolutionY: 1, positionX: 4, positionY: 0, width: 2, height: 1, value: 0xFF }, expected: [0b00001100] },
    { arg: { bytes: [0x00], resolutionX: 8, resolutionY: 1, positionX: 8, positionY: 0, width: 1, height: 1, value: 0xFF }, expected: [0x00] },
    // Multiple horizontal bytes
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 24, resolutionY: 1, positionX: 0, positionY: 0, width: 8, height: 1, value: 0xFF }, expected: [0xFF, 0x00, 0x00] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 24, resolutionY: 1, positionX: -10, positionY: 0, width: 800, height: 1, value: 0xFF }, expected: [0xFF, 0xFF, 0xFF] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 24, resolutionY: 1, positionX: 0, positionY: 0, width: 9, height: 1, value: 0xFF }, expected: [0xFF, 0b10000000, 0x00] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 24, resolutionY: 1, positionX: 4, positionY: 0, width: 9, height: 1, value: 0xFF }, expected: [0x0F, 0b11111000, 0x00] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 24, resolutionY: 1, positionX: 0, positionY: 0, width: 12, height: 1, value: 0xFF }, expected: [0xFF, 0xF0, 0x00] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 24, resolutionY: 1, positionX: 0, positionY: 0, width: 18, height: 1, value: 0xFF }, expected: [0xFF, 0xFF, 0b11000000] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 24, resolutionY: 1, positionX: 2, positionY: 0, width: 8, height: 1, value: 0xFF }, expected: [0b00111111, 0b11000000, 0x00] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 24, resolutionY: 1, positionX: 2, positionY: 0, width: 18, height: 1, value: 0xFF }, expected: [0b00111111, 0xFF, 0xF0] },
    // Vertical bytes
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 8, resolutionY: 3, positionX: 0, positionY: 0, width: 8, height: 1, value: 0xFF }, expected: [0xFF, 0x00, 0x00] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 8, resolutionY: 3, positionX: 4, positionY: 0, width: 8, height: 1, value: 0xFF }, expected: [0x0F, 0x00, 0x00] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 8, resolutionY: 3, positionX: 0, positionY: 0, width: 8, height: 3, value: 0xFF }, expected: [0xFF, 0xFF, 0xFF] },
    { arg: { bytes: [0x00, 0x00, 0x00], resolutionX: 8, resolutionY: 3, positionX: 4, positionY: 0, width: 8, height: 3, value: 0xFF }, expected: [0x0F, 0x0F, 0x0F] },
  ];

  tests.forEach(({ arg, expected }) => {
    const { bytes, resolutionX, resolutionY, width, height, positionX, positionY, value } = arg;
    it(`should fill bytes for ${JSON.stringify(arg)}`, async () => {
      fillRect(bytes, resolutionX, resolutionY, positionX, positionY, width, height, value);
      expect(bytes).to.deep.equal(expected);
    });
  });
});
