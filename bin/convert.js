// eslint-disable-next-line import/no-extraneous-dependencies
const Jimp = require('jimp');

const { create, FILL } = require('../src/drawing/util/raw_image.js');
const { setBit } = require('../src/drawing/util/bit_accessor.js');

const loadImage = async (inputFile) => Jimp.read(inputFile);

const resizeImage = (image, desiredWidth, desiredHeight) => {
  const jimpWidth = (desiredWidth === undefined || desiredWidth <= 0) ? Jimp.AUTO : desiredWidth;
  const jimpHeight = (desiredHeight === undefined || desiredHeight <= 0) ? Jimp.AUTO : desiredHeight;

  if (jimpWidth === Jimp.AUTO && jimpHeight === Jimp.AUTO) {
    return image;
  }

  return image.resize(jimpWidth, jimpHeight, Jimp.RESIZE_NEAREST_NEIGHBOR);
};

const getCanvasColors = (pixelColor) => {
  // pixels are red green blue alpha in hex.
  const isBlack = pixelColor === 0x000000FF;
  const isRed = pixelColor === 0xFF0000FF;

  const blackValue = isBlack ? FILL.ZERO : FILL.ONE;
  const redValue = isRed ? FILL.ZERO : FILL.ONE;

  return { blackValue, redValue };
};

const buildPainting = (image) => {
  const resolutionX = image.bitmap.width;
  const resolutionY = image.bitmap.height;

  const blackImage = create(resolutionX, resolutionY, FILL.ONE);
  const redImage = create(resolutionX, resolutionY, FILL.ONE);

  for (let x = 0; x < resolutionX; x += 1) {
    for (let y = 0; y < resolutionY; y += 1) {
      const pixelColor = image.getPixelColor(x, y);
      const canvasColor = getCanvasColors(pixelColor);

      setBit(blackImage, resolutionX, resolutionY, x, y, canvasColor.blackValue);
      setBit(redImage, resolutionX, resolutionY, x, y, canvasColor.redValue);
    }
  }

  return {
    blackCanvas: { image: blackImage, resolutionX, resolutionY },
    redCanvas: { image: redImage, resolutionX, resolutionY },
  };
};

const makePrintableCanvas = ({ image, resolutionX, resolutionY }) => `{
    image: new Uint8Array([${image.join(', ')}]),
    resolutionX: ${resolutionX},
    resolutionY: ${resolutionY},
  }`;

// Probably better ways of doing this printing, but I don't care :D!
const makePrintablePainting = (painting) => `/* eslint-disable max-len */
module.exports = {
  blackCanvas: ${makePrintableCanvas(painting.blackCanvas)},
  redCanvas: ${makePrintableCanvas(painting.redCanvas)},
};`;

/**
 * @returns a string of javascript that represents a canvas (image and resolution) for printing to an e-ink display
 *
 * @param {*} input the location on disk to load the image file from
 * @param {*} width [optional] the desired output width of the canvas
 * @param {*} height [optional] the desired output height of the canvas
 */
module.exports.convert = async (input, width, height) => {
  const image = await loadImage(input);
  const resizedImage = resizeImage(image, width, height);

  const painting = buildPainting(resizedImage);
  const printablePainting = makePrintablePainting(painting);

  return printablePainting;
};
