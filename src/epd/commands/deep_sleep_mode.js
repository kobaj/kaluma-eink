const { sendCommand, sendData } = require('../chip/transfer.js');

const DEEP_SLEEP_MODE = 0x10;

const DEEP_SLEEP_MODE_ARGS = {
  DATA_0: {
    CONTROL: {
      NORMAL_MODE: 0b00,
      ENTER_SLEEP: 0b01,
      // Docs mention a deep sleep mode 2, but its not clear what that means.
    },
  },
};

/**
 * Set the display to sleep.
 *
 * This should be called no matter what after the image is drawn,
 * failure to do so can cause the screen to burn up.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 */
module.exports.deepSleepMode = (connection, spi) => {
  sendCommand(DEEP_SLEEP_MODE, connection.dcPin, connection.csPin, spi);
  sendData(DEEP_SLEEP_MODE_ARGS.DATA_0.CONTROL.ENTER_SLEEP, connection.dcPin, connection.csPin, spi);
};
