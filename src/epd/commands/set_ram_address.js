const { getLeastSignificantBits, getMostSignificantBits } = require('../chip/address.js');
const { sendCommand, sendData } = require('../chip/transfer.js');

const SET_RAM_X_ADDRESS_BOUNDS = 0x44;
const SET_RAM_Y_ADDRESS_BOUNDS = 0x45;
const SET_RAM_X_ADDRESS = 0x4E;
const SET_RAM_Y_ADDRESS = 0x4F;

const validatePointer = (name, start, end, pointer) => {
  if (end < pointer || pointer < start) {
    throw new Error(`${name} ${pointer} needs to be between ${start} and ${end}.`);
  }
};

const validateBound = (name, bound) => {
  if (bound === undefined || !Number.isInteger(bound) || bound < 0) {
    throw new Error(`${name} ${bound} needs to be a positive integer.`);
  }
};

const validateOrder = (nameA, boundA, nameB, boundB) => {
  if (boundA > boundB) {
    throw new Error(`${nameA} ${boundA} needs to be less than or equal to ${nameB} ${boundB}.`);
  }
};

const validateOnlyOne = (nameA, boundA, nameB, boundB) => {
  if (boundA !== undefined && boundB !== undefined) {
    throw new Error(`Only one of ${nameA} ${boundA} or ${nameB} ${boundB} should be set.`);
  }

  if (boundA === undefined && boundB === undefined) {
    throw new Error(`One of ${nameA} or ${nameB} should be set.`);
  }
};

const getFullBoundary = (boundary) => {
  validateOnlyOne('endX', boundary.endX, 'resolutionX', boundary.resolutionX);
  validateOnlyOne('endY', boundary.endY, 'resolutionY', boundary.resolutionY);

  const { startX, startY } = boundary;
  const endX = boundary.endX ?? (Math.ceil(boundary.resolutionX / 8) - 1 + boundary.startX);
  const endY = boundary.endY ?? (boundary.resolutionY - 1 + boundary.startY);

  validateBound('startX', startX);
  validateBound('startY', startY);
  validateBound('endX', endX);
  validateBound('endY', endY);

  validateOrder('startX', startX, 'endX', endX);
  validateOrder('startY', startY, 'endY', endY);

  const deltaX = endX - startX;
  const deltaY = endY - startY;
  validateOrder('(endX - startX)', deltaX, '(endY - startY)', deltaY);

  return {
    startX, startY, endX, endY,
  };
};

const setRamAddress = (connection, spi, {
  startX, startY, endX, endY,
}) => {
  sendCommand(SET_RAM_X_ADDRESS_BOUNDS, connection.dcPin, connection.csPin, spi);
  sendData(startX, connection.dcPin, connection.csPin, spi); // start
  sendData(endX, connection.dcPin, connection.csPin, spi); // end

  sendCommand(SET_RAM_Y_ADDRESS_BOUNDS, connection.dcPin, connection.csPin, spi);
  sendData(getMostSignificantBits(startY), connection.dcPin, connection.csPin, spi); // start MSB
  sendData(getLeastSignificantBits(startY), connection.dcPin, connection.csPin, spi); // start LSB
  sendData(getMostSignificantBits(endY), connection.dcPin, connection.csPin, spi); // end MSB
  sendData(getLeastSignificantBits(endY), connection.dcPin, connection.csPin, spi); // end LSB
};

const setRamPointer = (connection, spi, { pointerX, pointerY }) => {
  sendCommand(SET_RAM_X_ADDRESS, connection.dcPin, connection.csPin, spi);
  sendData(pointerX, connection.dcPin, connection.csPin, spi); // start

  sendCommand(SET_RAM_Y_ADDRESS, connection.dcPin, connection.csPin, spi);
  sendData(getMostSignificantBits(pointerY), connection.dcPin, connection.csPin, spi); // start MSB
  sendData(getLeastSignificantBits(pointerY), connection.dcPin, connection.csPin, spi); // start LSB
};

/**
 * Sets the ram pointer in preperation for writing data.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 * @param {Object} boundary the address boundaries of the display
 * @param {number} boundary.startX the first address of the display x axis ram
 * @param {number} boundary.startY the first address of the display y axis ram
 * @param {number} boundary.endX the last address of the display x axis ram, only needed if you don't set resolutionX
 * @param {number} boundary.endY the last address of the display y axis ram, only needed if you don't set resolutionY
 * @param {number} boundary.resolutionX the number of pixels in the x axis of the display
 * @param {number} boundary.resolutionY the number of pixels in the y axis of the display, the y axis is whichever axis is bigger on the display
 * @param {Object} pointer the address where the first bit should be placed
 * @param {number} pointer.pointerX the x axis address that should be accessed by the next writeRam call
 * @param {number} pointer.pointerY the y axis address that should be accessed by the next writeRam call
 */
module.exports.setRamPointer = (connection, spi, boundary, pointer) => {
  const { pointerX, pointerY } = pointer;
  const {
    startX, startY, endX, endY,
  } = getFullBoundary(boundary);

  validatePointer('pointerX', startX, endX, pointerX);
  validatePointer('pointerY', startY, endY, pointerY);

  setRamPointer(connection, spi, pointer);
};

/**
 * Resets the ram address bounds and pointers in preperation for writing data.
 *
 * By default this will set the pointers to the start boundary values.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 * @param {Object} boundary the address boundaries of the display
 * @param {number} boundary.startX the first address of the display x axis ram
 * @param {number} boundary.startY the first address of the display y axis ram
 * @param {number} boundary.endX the last address of the display x axis ram, only needed if you don't set resolutionX
 * @param {number} boundary.endY the last address of the display y axis ram, only needed if you don't set resolutionY
 * @param {number} boundary.resolutionX the number of pixels in the x axis of the display
 * @param {number} boundary.resolutionY the number of pixels in the y axis of the display, the y axis is whichever axis is bigger on the display
 */
module.exports.setRamAddress = (connection, spi, boundary) => {
  const fullBoundary = getFullBoundary(boundary);
  setRamAddress(connection, spi, fullBoundary);
  setRamPointer(connection, spi, { pointerX: fullBoundary.startX, pointerY: fullBoundary.startY });
};
