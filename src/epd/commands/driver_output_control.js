const { getMostSignificantBits, getLeastSignificantBits } = require('../chip/address.js');
const { sendCommand, sendData } = require('../chip/transfer.js');

const DRIVER_OUTPUT_CONTROL = 0x01;

const DRIVER_OUTPUT_CONTROL_ARGS = {
  // DATA_0 and DATA_1 are user supplied MUX values
  DATA_2: {
    UNKNOWN: {
      POR: 0x00,
    },
  },
};

const driverOutputControl = (connection, spi, { resolutionY }) => {
  if (!resolutionY || !Number.isInteger(resolutionY) || resolutionY <= 1) {
    throw new Error(`ResolutionY ${resolutionY} needs to be a positive integer greater than 1.`);
  }

  sendCommand(DRIVER_OUTPUT_CONTROL, connection.dcPin, connection.csPin, spi);

  const yAddress = resolutionY - 1;
  sendData(getMostSignificantBits(yAddress), connection.dcPin, connection.csPin, spi);
  sendData(getLeastSignificantBits(yAddress), connection.dcPin, connection.csPin, spi);
  sendData(DRIVER_OUTPUT_CONTROL_ARGS.DATA_2.UNKNOWN.POR, connection.dcPin, connection.csPin, spi);
};

/**
 * Set how big the display is.
 *
 * By default this will set the B gate setting to zero, whatever that means.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 * @param {Object} display some properties of the display
 */
module.exports.driverOutputControl = (connection, spi, display) => {
  if (display.resolutionY !== undefined && display.endY !== undefined) {
    throw new Error(`Only one of resolutionY ${display.resolutionY} or endY ${display.endY} should be set.`);
  }

  if (display.resolutionY === undefined && display.endY === undefined) {
    throw new Error('One of resolutionY or endY should be set.');
  }

  const resolutionY = display.resolutionY ?? (display.endY - display.startY + 1);
  driverOutputControl(connection, spi, { resolutionY });
};
