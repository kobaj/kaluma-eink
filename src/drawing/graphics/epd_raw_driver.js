/* eslint-disable no-restricted-syntax */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
const { displayFrame, displayFrameSync } = require('../../epd/epd_generic.js');

const shouldClear = (data, epd) => {
  const clearLength = Math.ceil(epd.resolutionX / 8) * epd.resolutionY;
  const missingBlackData = epd.supportsBlackImage && data.blackImage && data.blackImage.length < clearLength;
  const missingRedData = epd.supportsRedImage && data.redImage && data.redImage.length < clearLength;
  return missingBlackData || missingRedData;
};

module.exports.EpdRawDriver = class {
  constructor() {
    this.displayData = this.displayData.bind(this);
  }

  setup(connection, spi, epd) {
    this.connection = connection;
    this.spi = spi;
    this.epd = epd;

    if (epd === undefined) {
      throw new Error('EPD must be defined.');
    }

    if (connection === undefined || spi === undefined) {
      throw new Error('Connection and SPI need to be defined.');
    }
  }

  /**
   * Render the data directly to the display without any buffering or manipulation.
   *
   * @param {*} data an object that defines all data to be sent
   * @param {*} data.blackImage the array of bytes to send to the display, 0 for black and 1 for white
   * @param {*} data.redImage the array of bytes to send to the display, 0 for red and 1 for white
   */
  displayData(data) {
    const clear = shouldClear(data, this.epd);
    displayFrameSync(this.connection, this.spi, data, { clear }, this.epd);
  }

  /**
   * Async version of displayData.
   */
  async displayDataAsync(data) {
    const clear = shouldClear(data, this.epd);
    await displayFrame(this.connection, this.spi, data, { clear }, this.epd);
  }
};
