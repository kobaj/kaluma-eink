const chai = require('chai');

const sinon = require('sinon');

const { stubGpio, stubSpi } = require('../../../fixtures/kaluma.js');

const { sendCommand, sendData } = require('./transfer.js');

const { expect } = chai;

describe('transfer', () => {
  const dcPin = stubGpio(1, OUTPUT);
  const csPin = stubGpio(2, OUTPUT);
  const spi = stubSpi();

  beforeEach('reset', () => {
    sinon.reset();
  });

  describe('sendCommand', () => {
    describe('call', () => {
      const tests = [
        { arg: { command: 0xFF, dcPin, csPin, spi }, expected: new Uint8Array([0xFF]) },
        { arg: { command: 0x01, dcPin, csPin, spi }, expected: new Uint8Array([0x01]) },
        { arg: { command: [0xFF], dcPin, csPin, spi }, expected: new Uint8Array([0xFF]) },
        { arg: { command: new Uint8Array([0xFF]), dcPin, csPin, spi }, expected: new Uint8Array([0xFF]) },
      ];

      tests.forEach(({ arg, expected }) => {
        it(`should succeed for input ${JSON.stringify(arg)}`, async () => {
          sendCommand(arg.command, arg.dcPin, arg.csPin, arg.spi);

          expect(dcPin.write).to.be.calledWith(LOW);
          expect(spi.send).calledWith(expected);
        });
      });
    });
  });

  describe('sendData', () => {
    describe('call', () => {
      const tests = [
        { arg: { data: [0xFF, 0x11, 0x00], dcPin, csPin, spi }, expected: new Uint8Array([0xFF, 0x11, 0x00]) },
        { arg: { data: new Uint8Array([0xFF, 0x11, 0x00]), dcPin, csPin, spi }, expected: new Uint8Array([0xFF, 0x11, 0x00]) },
      ];

      tests.forEach(({ arg, expected }) => {
        it(`should succeed for input ${JSON.stringify(arg)}`, async () => {
          sendData(arg.data, arg.dcPin, arg.csPin, arg.spi);

          expect(dcPin.write).to.be.calledWith(HIGH);
          expect(spi.send).calledWith(expected);
        });
      });
    });
  });
});
