const chai = require('chai');

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const { stubSpi, stubGpio } = require('../../../fixtures/kaluma.js');
const { counter, counterReset } = require('../../../fixtures/next_call.js');

const { writeRam } = require('./write_ram.js');

chai.use(sinonChai);
const { expect } = chai;

describe('write_ram', () => {
  const connection = {
    csPin: stubGpio(1, OUTPUT),
    dcPin: stubGpio(1, OUTPUT),
    resetPin: stubGpio(1, OUTPUT),
    busyPin: stubGpio(1, INPUT),
  };
  const spi = stubSpi();
  const tCount = counter();

  beforeEach('reset', () => {
    sinon.reset();
    counterReset();
  });

  describe('call', () => {
    it('should succeed with writing black data', async () => {
      const blackImage = [0x00, 0xFF, 0xAA];
      writeRam(connection, spi, { blackImage });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x24]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(blackImage));
    });

    it('should succeed with writing red data', async () => {
      const redImage = [0x00, 0xFF, 0xAA];
      writeRam(connection, spi, { redImage });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x26]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(redImage));
    });

    it('should succeed with writing black and red data', async () => {
      const blackImage = [0x00, 0xFF, 0xAA];
      const redImage = [0xAA, 0xBB, 0xCC];
      writeRam(connection, spi, { blackImage, redImage });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x24]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(blackImage));

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x26]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(redImage));
    });

    it('should succeed with writing black and red data from generators', async () => {
      const generateBlackImage = function* generateBlackImage() {
        yield 0x00;
        yield 0xFF;
        yield 0xAA;
      };

      const generateRedImage = function* generateRedImage() {
        yield 0xAA;
        yield 0xBB;
        yield 0xCC;
      };

      writeRam(connection, spi, { blackImage: generateBlackImage(), redImage: generateRedImage() });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x24]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x00, 0xFF, 0xAA]));

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x26]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0xAA, 0xBB, 0xCC]));
    });

    it('should succeed with writing lots of black and red data', async () => {
      const blackImage = new Array(200).fill(0xFF);
      const redImage = new Array(120).fill(0xAA);
      writeRam(connection, spi, { blackImage, redImage });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x24]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(new Array(100).fill(0xFF)));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(new Array(100).fill(0xFF)));

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x26]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(new Array(100).fill(0xAA)));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(new Array(20).fill(0xAA)));
    });

    it('should succeed with writing lots of black and red data from generators', async () => {
      const generateBlackImage = function* generateBlackImage() {
        for (let x = 0; x < 200; x += 1) {
          yield 0xAA;
        }
      };

      const generateRedImage = function* generateRedImage() {
        for (let x = 0; x < 120; x += 1) {
          yield 0xBB;
        }
      };

      writeRam(connection, spi, { blackImage: generateBlackImage(), redImage: generateRedImage() });

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x24]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(new Array(100).fill(0xAA)));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(new Array(100).fill(0xAA)));

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x26]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(new Array(100).fill(0xBB)));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array(new Array(20).fill(0xBB)));
    });
  });
});
