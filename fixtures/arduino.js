/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-extraneous-dependencies */
const sinon = require('sinon');

global.digitalWrite = sinon.stub();
global.digitalRead = sinon.stub();
global.delay = sinon.stub();

global.LOW = 0;
global.HIGH = 1;

global.INPUT = 0x0;
global.OUTPUT = 0x1;
global.INPUT_PULLUP = 0x2;
global.INPUT_PULLDOWN = 0x3;
global.OUTPUT_OPENDRAIN = 0x4;
