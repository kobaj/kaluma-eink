const { sendCommand, sendData } = require('../chip/transfer.js');

const WRITE_RAM_BLACK = 0x24;
const WRITE_RAM_RED = 0x26;

/**
 * Writes image data to the display ram.
 *
 * Data is an array of bytes, where one bit is one pixel. A 1 represents white and 0 represents color.
 * A display of size 16 pixels wide (x resolution) by 4 pixels tall (y resolution) would therefore
 * have data in an array that looked like this:
 *
 * ```
 * [
 *   0b10000000, 0b00000000, // <-- first bit in the top left corner is white, all other bits are color
 *   0b00000000, 0b00000000,
 *   0b00000000, 0b00000000,
 *   0b00000000, 0b00000000,
 * ]
 * ```
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 * @param {Object} data the data to display
 * @param {number[]} data.blackImage the black bits of the data to display
 * @param {number[]} data.redImage the red bits of the data to display
 */
module.exports.writeRam = (connection, spi, { blackImage, redImage }) => {
  if (blackImage) {
    sendCommand(WRITE_RAM_BLACK, connection.dcPin, connection.csPin, spi);
    sendData(blackImage, connection.dcPin, connection.csPin, spi);
  }

  if (redImage) {
    sendCommand(WRITE_RAM_RED, connection.dcPin, connection.csPin, spi);
    sendData(redImage, connection.dcPin, connection.csPin, spi);
  }
};
