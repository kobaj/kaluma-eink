const { readPin } = require('../accessors/gpio.js');
const accTimeout = require('../accessors/timeout.js');

const DELAY_MS = 10;
const DEFAULT_DELAY_MS = 2000;

const isBusy = (busyPin) => readPin(busyPin) === HIGH;

/**
 * Wait for the display to no longer be busy synchronously, this will block.
 *
 * @param {(number|Object)} busyPin that will become LOW when not busy
 * @param {number} timeout how long to wait before throwing an error
 */
module.exports.untilIdleSync = (busyPin, timeout = DEFAULT_DELAY_MS) => {
  let accruedTime = 0;
  while (isBusy(busyPin)) {
    if (accruedTime > timeout) {
      throw new Error('Timed out waiting for display to be ready.');
    }

    delay(DELAY_MS);
    accruedTime += DELAY_MS;
  }
};

/**
 * Async version of untilIdle, see untilIdleSync for documentation.
 */
module.exports.untilIdle = async (busyPin, timeout = DEFAULT_DELAY_MS) => {
  let accruedTime = 0;
  while (isBusy(busyPin)) {
    if (accruedTime > timeout) {
      throw new Error('Timed out waiting for display to be ready.');
    }

    // eslint-disable-next-line no-await-in-loop
    await accTimeout.timeout(DELAY_MS);
    accruedTime += DELAY_MS;
  }
};
