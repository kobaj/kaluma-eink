const chai = require('chai');

const { getBit, setBit } = require('./bit_accessor.js');

const { expect } = chai;

describe('bit_accessor', () => {

  describe('getBit', () => {
    const tests = [
      // Single byte
      { arg: { bytes: [0xFF], width: 8, height: 1, positionX: 0, positionY: 0 }, expected: 0x01 },
      { arg: { bytes: [0x00], width: 8, height: 1, positionX: 0, positionY: 0 }, expected: 0x00 },
      { arg: { bytes: [0x0F], width: 8, height: 1, positionX: 0, positionY: 0 }, expected: 0x00 },
      { arg: { bytes: [0x0F], width: 8, height: 1, positionX: 7, positionY: 0 }, expected: 0x01 },
      // Multiple bytes
      { arg: { bytes: [0x0F, 0xF0], width: 16, height: 1, positionX: 8, positionY: 0 }, expected: 0x01 },
      { arg: { bytes: [0x0F, 0xF0], width: 16, height: 1, positionX: 15, positionY: 0 }, expected: 0x00 },
    ];

    tests.forEach(({ arg, expected }) => {
      const { bytes, width, height, positionX, positionY } = arg;
      it(`should get the bit of ${JSON.stringify(arg)}`, async () => {
        expect(getBit(bytes, width, height, positionX, positionY)).to.equal(expected);
      });
    });

    it('should return undefined for a wild position.', () => {
      expect(getBit([0xFF], 1, 1, -100, -100)).to.be.undefined;
      expect(getBit([0xFF], 1, 1, 100, 100)).to.be.undefined;
    });
  });

  describe('setBit', () => {
    const tests = [
      // Single byte
      { arg: { bytes: [0xFF], width: 8, height: 1, positionX: 0, positionY: 0, value: 0x01 }, expected: [0b11111111] },
      { arg: { bytes: [0xFF], width: 8, height: 1, positionX: 0, positionY: 0, value: 0x00 }, expected: [0b01111111] },
      { arg: { bytes: [0xFF], width: 8, height: 1, positionX: 7, positionY: 0, value: 0x00 }, expected: [0b11111110] },
      // Single off byte
      { arg: { bytes: [0x00], width: 8, height: 1, positionX: 0, positionY: 0, value: 0x01 }, expected: [0b10000000] },
      { arg: { bytes: [0x00], width: 8, height: 1, positionX: 0, positionY: 0, value: 0x00 }, expected: [0b00000000] },
      { arg: { bytes: [0x00], width: 8, height: 1, positionX: 7, positionY: 0, value: 0x01 }, expected: [0b00000001] },
      // Multiple bytes
      { arg: { bytes: [0x00, 0xFF], width: 16, height: 1, positionX: 0, positionY: 0, value: 0x01 }, expected: [0b10000000, 0b11111111] },
      { arg: { bytes: [0x00, 0xFF], width: 16, height: 1, positionX: 15, positionY: 0, value: 0x00 }, expected: [0b00000000, 0b11111110] },
    ];

    tests.forEach(({ arg, expected }) => {
      const { bytes, width, height, positionX, positionY, value } = arg;
      it(`should set the bit of ${JSON.stringify(arg)}`, async () => {
        setBit(bytes, width, height, positionX, positionY, value);
        expect(bytes).to.deep.equal(expected);
      });
    });
  });
});
