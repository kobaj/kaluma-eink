const chai = require('chai');

const { buildClearData } = require('./clear.js');

const { expect } = chai;

describe('clear', () => {
  describe('buildClearData', () => {
    describe('bad input', () => {
      const tests = [
        {},
        { startX: 0, endX: 100, resolutionX: 100, startY: 0, resolutionY: 100 },
        { startX: 0, resolutionX: 100, startY: 0, endY: 100, resolutionY: 100 },
        { startX: 0, resolutionX: 100, startY: 0, resolutionY: -100 },
        { startX: 0, resolutionX: -100, startY: 0, resolutionY: 100 },
        { startX: 0, end: -100, startY: 0, endY: 100 },
        { startX: 0, end: 100, startY: 0, endY: -100 },
      ];

      tests.forEach((arg) => {
        it(`should throw exception for ${JSON.stringify(arg)}`, async () => {
          expect(() => buildClearData(arg)).to.throw();
        });
      });
    });

    describe('call', () => {
      const tests = [
        { arg: { startX: 0, endX: 0, startY: 0, endY: 0, supportsBlackImage: true }, expected: { blackImage: new Uint8Array([0xFF]) } },
        { arg: { startX: 0, endX: 1, startY: 0, endY: 1, supportsBlackImage: true }, expected: { blackImage: new Uint8Array([0xFF, 0xFF, 0xFF, 0xFF]) } },
        { arg: { startX: 1, endX: 1, startY: 1, endY: 1, supportsBlackImage: true }, expected: { blackImage: new Uint8Array([0xFF]) } },
        { arg: { startX: 1, endX: 2, startY: 1, endY: 2, supportsBlackImage: true }, expected: { blackImage: new Uint8Array([0xFF, 0xFF, 0xFF, 0xFF]) } },
        // Resolution
        { arg: { resolutionX: 8, resolutionY: 1, supportsBlackImage: true }, expected: { blackImage: new Uint8Array([0xFF]) } },
        { arg: { resolutionX: 16, resolutionY: 2, supportsBlackImage: true }, expected: { blackImage: new Uint8Array([0xFF, 0xFF, 0xFF, 0xFF]) } },
        { arg: { resolutionX: 200, resolutionY: 200, supportsBlackImage: true }, expected: { blackImage: new Uint8Array(new Array(5000).fill(0xFF)) } },
        { arg: { resolutionX: 152, resolutionY: 296, supportsBlackImage: true }, expected: { blackImage: new Uint8Array(new Array(5624).fill(0xFF)) } },
      ];

      tests.forEach(({ arg, expected }) => {
        it(`should succeed for input ${JSON.stringify(arg)}`, async () => {
          const output = buildClearData(arg);

          expect(output).property('blackImage').to.exist;
          expect(output).property('redImage').to.not.exist;
          expect(Uint8Array.from(output.blackImage)).to.deep.equal(expected.blackImage);
        });
      });

      it('should not produce anything for empty input', async () => {
        const output = buildClearData({ startX: 0, endX: 100, startY: 0, endY: 100 });

        expect(output).property('redImage').to.not.exist;
        expect(output).property('blackImage').to.not.exist;
      });

      it('should succeed for red input', async () => {
        const output = buildClearData({ resolutionX: 8, resolutionY: 1, supportsRedImage: true });

        expect(output).property('redImage').to.exist;
        expect(output).property('blackImage').to.not.exist;
        expect(Uint8Array.from(output.redImage)).to.deep.equal(new Uint8Array([0xFF]));
      });

      it('should succeed for black and red input', async () => {
        const output = buildClearData({ resolutionX: 8, resolutionY: 1, supportsBlackImage: true, supportsRedImage: true });

        expect(output).property('redImage').to.exist;
        expect(output).property('blackImage').to.exist;
        expect(Uint8Array.from(output.redImage)).to.deep.equal(new Uint8Array([0xFF]));
        expect(Uint8Array.from(output.blackImage)).to.deep.equal(new Uint8Array([0xFF]));
      });
    });
  });
});
