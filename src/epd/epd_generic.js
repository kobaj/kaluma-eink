const { buildClearData } = require('./chip/clear.js');

const { deepSleepMode } = require('./commands/deep_sleep_mode.js');
const { swReset, swResetSync } = require('./commands/sw_reset.js');
const { hwReset, hwResetSync } = require('./commands/hw_reset.js');
const { driverOutputControl } = require('./commands/driver_output_control.js');
const { dataEntryMode } = require('./commands/data_entry_mode.js');
const { borderWaveformControl } = require('./commands/border_waveform_control.js');
const { setRamAddress, setRamPointer } = require('./commands/set_ram_address.js');
const { writeRam } = require('./commands/write_ram.js');
const { displayUpdateControl } = require('./commands/display_update_control.js');
const { mainActivation, mainActivationSync } = require('./commands/main_activation.js');
const { tempSensorSelection } = require('./commands/temp_sensor_selection.js');

const DEFAULT_OPTIONS = {
  clear: false,
};

const resetSync = (connection, spi) => {
  hwResetSync(connection, spi);
  swResetSync(connection, spi);
};

const reset = async (connection, spi) => {
  await hwReset(connection, spi);
  await swReset(connection, spi);
};

const sendInitializationCode = (connection, spi, display) => {
  driverOutputControl(connection, spi, display);
  dataEntryMode(connection, spi);
  borderWaveformControl(connection, spi);
  setRamAddress(connection, spi, display);
  displayUpdateControl(connection, spi, display);
  tempSensorSelection(connection, spi);
};

/**
 * Takes care of the entire process for updating an e-ink display synchronously, this will block.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {(Number|Object)} connection.csPin the cable select pin of SPI
 * @param {(Number|Object)} connection.dsPin the data/command select pin of the display
 * @param {(Number|Object)} connection.resetPin the reset pin of the display
 * @param {(Number|Object)} connection.busyPin the busy pin of the display
 * @param {Object} spi the spi connection to the display
 * @param {number[]} data the image to be shown on the display, see writeRam for documentation
 * @param {Object} options properties about how the display should show the image
 * @param {boolean} options.clear whether or not to clear the display prior to writing image data
 * @param {Object} display properties about the display
 * @param {number} display.startX the first address of the display x axis ram
 * @param {number} display.startY the first address of the display y axis ram
 * @param {number} display.endX the last address of the display x axis ram, only needed if you don't set resolutionX
 * @param {number} display.endY the last address of the display y axis ram, only needed if you don't set resolutionY
 * @param {number} display.resolutionX the number of pixels in the x axis of the display
 * @param {number} display.resolutionY the number of pixels in the y axis of the display, the y axis is whichever axis is bigger on the display
 * @param {boolean} display.supportsBlackImage indicating that there are black pixels in the display
 * @param {boolean} display.supportsRedImage indicating that there are red pixels in the display
 */
module.exports.displayFrameSync = (connection, spi, data, options = DEFAULT_OPTIONS, display = {}) => {
  resetSync(connection, spi);
  try {
    sendInitializationCode(connection, spi, display);

    let isRamDirty = false;
    if (options.clear) {
      const clearData = buildClearData(display);
      setRamPointer(connection, spi, display, { pointerX: display.startX, pointerY: display.startY });
      writeRam(connection, spi, clearData);
      isRamDirty = true;
    }

    if (data) {
      setRamPointer(connection, spi, display, { pointerX: display.startX, pointerY: display.startY });
      writeRam(connection, spi, data);
      isRamDirty = true;
    }

    if (isRamDirty) {
      mainActivationSync(connection, spi);
    }
  } catch (err) {
    resetSync(connection, spi);
    throw err;
  } finally {
    deepSleepMode(connection, spi);
  }
};

/**
 * Async version of displayFrame, see displayFrameSync method for documentation.
 */
module.exports.displayFrame = async (connection, spi, data, options = DEFAULT_OPTIONS, display = {}) => {
  await reset(connection, spi);
  try {
    sendInitializationCode(connection, spi, display);

    let isRamDirty = false;
    if (options.clear) {
      const clearData = buildClearData(display);
      setRamPointer(connection, spi, display, { pointerX: display.startX, pointerY: display.startY });
      writeRam(connection, spi, clearData);
      isRamDirty = true;
    }

    if (data) {
      setRamPointer(connection, spi, display, { pointerX: display.startX, pointerY: display.startY });
      writeRam(connection, spi, data);
      isRamDirty = true;
    }

    if (isRamDirty) {
      await mainActivation(connection, spi);
    }
  } catch (err) {
    await reset(connection, spi);
    throw err;
  } finally {
    deepSleepMode(connection, spi);
  }
};
