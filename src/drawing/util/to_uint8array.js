const { validateFreeMemory } = require('./memory.js');

/**
 * @returns a Uint8Array containing data
 *
 * @param {*} data which can be a single byte, an array of bytes, or a Uint8Array of bytes
 */
module.exports.toUint8Array = (data) => {
  if (data === undefined) {
    throw new Error('Data cannot be undefined');
  }

  if (data instanceof Uint8Array) {
    return data;
  }

  if (typeof data === 'string' || data instanceof String) {
    validateFreeMemory(data.length);
    return Uint8Array.from(data, (c) => c.charCodeAt(0));
  }

  if (Array.isArray(data)) {
    validateFreeMemory(data.length);
    return Uint8Array.from(data);
  }

  return new Uint8Array([data]);
};
