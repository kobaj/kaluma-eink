const applyState = (context, args) => {
  const state = args[args.length - 1];

  if (state.rotation !== undefined) {
    context.setRotation(state.rotation);
  }

  if (state.color !== undefined) {
    context.setColor(state.color);
  }

  if (state.fillColor !== undefined) {
    context.setFillColor(state.fillColor);
  }

  if (state.fontColor !== undefined) {
    context.setFontColor(state.fontColor);
  }

  if (state.font !== undefined) {
    context.setFont(state.font);
  }

  if (state.fontScaleX !== undefined || state.fontScaleY !== undefined) {
    const scaleX = state.fontScaleX ?? 1;
    const scaleY = state.fontScaleY ?? 1;
    context.setFontScale(scaleX, scaleY);
  }
};

module.exports.apply = (context) => {
  // eslint-disable-next-line no-underscore-dangle
  if (context._isStateless) {
    return context;
  }

  const methods = ['drawLine', 'drawRect', 'fillRect', 'drawCircle', 'fillCircle', 'drawRoundRect', 'fillRoundRect', 'drawText', 'drawBitmap'];
  methods.forEach((method) => {
    const ogMethod = context[method];
    context[`_${method}`] = ogMethod;
    context[method] = (...args) => {
      applyState(context, args);
      return ogMethod(...args);
    };
  });

  // eslint-disable-next-line no-underscore-dangle
  context._isStateless = true;
  return context;
};
