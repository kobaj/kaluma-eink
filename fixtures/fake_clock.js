// eslint-disable-next-line import/no-extraneous-dependencies
const sinon = require('sinon');

let setTimeoutStub;

module.exports.stubSetTimeout = () => {
  if (setTimeoutStub) {
    throw new Error('Attempted to stub setTimeout, but it was not properly reset last time. Did you forget to call restoreClock?');
  }

  setTimeoutStub = sinon.stub(global, 'setTimeout');
  setTimeoutStub.callsFake((cb) => cb());
};

module.exports.restoreClock = () => {
  setTimeoutStub.restore();
  setTimeoutStub = undefined;
};
