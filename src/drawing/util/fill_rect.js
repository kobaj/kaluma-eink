const { setBit } = require('./bit_accessor.js');
const { setByte } = require('./byte_indexer.js');

const multipleOf8 = (value) => (value + 7) & ~0x07;

const fillRectUnaligned = (bytes, resolutionX, resolutionY, posX, posY, width, height, value) => {
  const endX = posX + width;
  const endY = posY + height;
  for (let x = posX; x < endX; x += 1) {
    for (let y = posY; y < endY; y += 1) {
      setBit(bytes, resolutionX, resolutionY, x, y, value);
    }
  }
};

const fillRectAligned = (bytes, resolutionX, resolutionY, posX, posY, width, height, value) => {
  const endX = posX + width;
  const endY = posY + height;

  for (let x = posX; x < endX; x += 8) {
    for (let y = posY; y < endY; y += 1) {
      setByte(bytes, resolutionX, resolutionY, x, y, value);
    }
  }
};

const fillRectAll = (bytes, resolutionX, resolutionY, posX, posY, width, height, value) => {
  if (width <= 8) {
    fillRectUnaligned(bytes, resolutionX, resolutionY, posX, posY, width, height, value);
    return;
  }

  // start
  const startX = posX;
  const midStartX = multipleOf8(posX);
  fillRectUnaligned(bytes, resolutionX, resolutionY, startX, posY, midStartX - startX, height, value);

  // middle
  const endX = posX + width;
  const midEndX = multipleOf8(endX - 7);
  fillRectAligned(bytes, resolutionX, resolutionY, midStartX, posY, midEndX - midStartX, height, value);

  // end
  fillRectUnaligned(bytes, resolutionX, resolutionY, midEndX, posY, endX - midEndX, height, value);
};

/**
 * Fills bytes with value as indicated between the rectangle formed between positionX/positionY and width/height
 *
 * @param {*} bytes the bytes to write a bit to, this will modify the array
 * @param {*} resolutionX the number of bits in a row
 * @param {*} resolutionY the number of rows
 * @param {*} positionX the start x-bit location for filling
 * @param {*} positionY the start y-bit location for filling
 * @param {*} width the number of bits to fill horizontally
 * @param {*} height the number of bits to fill vertically
 * @param {*} value of either 0x00 or 0x01 that will be set at the specified position
 */
module.exports.fillRect = (bytes, resolutionX, resolutionY, posX, posY, width, height, value) => {
  if (width <= 0 || height <= 0) {
    throw new Error(`Width ${width} and height ${height} must be greater than zero.`);
  }

  if (!Number.isSafeInteger(value) || value < 0 || value > 255) {
    throw new Error(`Value ${value} must be an integer between 0 and 255`);
  }

  const startX = Math.max(0, posX);
  const startY = Math.max(0, posY);

  const endX = Math.min(resolutionX, posX + width);
  const endY = Math.min(resolutionY, posY + height);

  fillRectAll(bytes, resolutionX, resolutionY, startX, startY, endX - startX, endY - startY, value);
};
