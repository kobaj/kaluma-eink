const chai = require('chai');

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const { stubSpi, stubGpio } = require('../../../fixtures/kaluma.js');
const { counter, counterReset } = require('../../../fixtures/next_call.js');

const { setRamAddress } = require('./set_ram_address.js');

chai.use(sinonChai);
const { expect } = chai;

describe('set_ram_address', () => {
  const connection = {
    csPin: stubGpio(1, OUTPUT),
    dcPin: stubGpio(1, OUTPUT),
    resetPin: stubGpio(1, OUTPUT),
    busyPin: stubGpio(1, INPUT),
  };
  const spi = stubSpi();
  const tCount = counter();

  beforeEach('reset', () => {
    sinon.reset();
    counterReset();
  });

  describe('bounds checks', () => {
    const tests = [
      // Negative bounds
      { startX: -1, startY: 0, endX: 100, endY: 100 },
      { startX: 0, startY: -1, endX: 100, endY: 100 },
      { startX: 0, startY: 0, endX: -100, endY: 100 },
      { startX: 0, startY: 0, endX: 100, endY: -100 },

      // swapped bounds
      { startX: 100, startY: 0, endX: 0, endY: 100 },
      { startX: 0, startY: 100, endX: 100, endY: 0 },

      // x y wrong order
      { startX: 0, startY: 0, endX: 200, endY: 100 },

      // mixed resolution and ends
      { startX: 0, startY: 0, endX: 100, resolutionX: 100, endY: 100 },
      { startX: 0, startY: 0, endX: 100, endY: 100, resolutionY: 100 },
      { startX: 0, startY: 0, endX: 100, endY: 100, resolutionY: 100 },
      { startX: 0, startY: 0, endY: 100 },
      { startX: 0, startY: 0, endX: 100 },
    ];

    tests.forEach((arg) => {
      it(`should throw error for bounds of ${JSON.stringify(arg)}`, async () => {
        expect(() => setRamAddress(connection, spi, arg)).to.throw();
      });
    });
  });

  describe('call', () => {
    const tests = [
      { arg: { startX: 0, resolutionX: 152, startY: 1, resolutionY: 296 }, expected: { startX: 0x00, endX: 0x12, startYmsb: 0x01, startYlsb: 0x00, endYmsb: 0x28, endYlsb: 0x01 } },
      { arg: { startX: 0, resolutionX: 152, startY: 0, resolutionY: 296 }, expected: { startX: 0x00, endX: 0x12, startYmsb: 0x00, startYlsb: 0x00, endYmsb: 0x27, endYlsb: 0x01 } },
      { arg: { startX: 1, resolutionX: 152, startY: 0, resolutionY: 296 }, expected: { startX: 0x01, endX: 0x13, startYmsb: 0x00, startYlsb: 0x00, endYmsb: 0x27, endYlsb: 0x01 } },
    ];

    tests.forEach(({ arg, expected }) => {
      it(`should succeed with bounds of ${JSON.stringify(arg)}`, async () => {
        setRamAddress(connection, spi, arg);

        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x44]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.startX]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.endX]));

        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x45]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.startYmsb]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.startYlsb]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.endYmsb]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.endYlsb]));

        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x4E]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.startX]));

        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x4F]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.startYmsb]));
        expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([expected.startYlsb]));
      });
    });
  });
});
