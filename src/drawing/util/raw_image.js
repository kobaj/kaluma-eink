const { validateFreeMemory } = require('./memory.js');

/**
 * Use as a fillValue for canvas method
 */
module.exports.FILL = {
  ZERO: 0x00, // Typically this means a black/colored pixel
  ONE: 0xFF, // And this means a white/uncolored pixel, do not change this to 0x01
};

/**
 * @returns an array of bytes to act as a blank canvas
 *
 * Note: This may have more bits than specified if the width is not a multiple of 8.
 *
 * @param {*} resolutionX the number of pixels in each row
 * @param {*} resolutionY the number of pixels in each column
 * @param {*} fillValue the value to set each bit
 */
module.exports.create = (resolutionX, resolutionY, fillValue) => {
  const amount = Math.ceil(resolutionX / 8) * Math.ceil(resolutionY);
  validateFreeMemory(amount);
  return new Uint8Array(amount).fill(fillValue);
};
