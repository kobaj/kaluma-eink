const chai = require('chai');
const Jimp = require('jimp');

const { convert } = require('./convert.js');

const { expect } = chai;

describe('convert', () => {
  it('should convert a file to a canvas js object', async () => {
    const image = new Jimp(2, 2, 0x000000FF);
    const canvas = await convert(image);

    expect(canvas).to.deep.equal(`/* eslint-disable max-len */
module.exports = {
  blackCanvas: {
    image: new Uint8Array([63, 63]),
    resolutionX: 2,
    resolutionY: 2,
  },
  redCanvas: {
    image: new Uint8Array([255, 255]),
    resolutionX: 2,
    resolutionY: 2,
  },
};`);
  });

  it('should convert and scale file to a canvas js object', async () => {
    const image = new Jimp(2, 2, 0x000000FF);
    const canvas = await convert(image, 4, 4);

    expect(canvas).to.deep.equal(`/* eslint-disable max-len */
module.exports = {
  blackCanvas: {
    image: new Uint8Array([15, 15, 15, 15]),
    resolutionX: 4,
    resolutionY: 4,
  },
  redCanvas: {
    image: new Uint8Array([255, 255, 255, 255]),
    resolutionX: 4,
    resolutionY: 4,
  },
};`);
  });

  it('should convert a file with red to a canvas js object', async () => {
    const image = new Jimp(2, 2, 0xFF0000FF);
    const canvas = await convert(image);

    expect(canvas).to.deep.equal(`/* eslint-disable max-len */
module.exports = {
  blackCanvas: {
    image: new Uint8Array([255, 255]),
    resolutionX: 2,
    resolutionY: 2,
  },
  redCanvas: {
    image: new Uint8Array([63, 63]),
    resolutionX: 2,
    resolutionY: 2,
  },
};`);
  });
});
