const { sendCommand, sendData } = require('../chip/transfer.js');

const TEMPERATURE_SENSOR_SELECTION = 0x18;

const TEMPERATURE_SENSOR_SELECTION_ARGS = {
  DATA_0: {
    SOURCE: {
      EXTERNAL: 0x48,
      INTERNAL: 0x80,
    },
  },
};

/**
 * Set the source of temperature data for the display.
 *
 * By default this will set the source to be internal.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 */
module.exports.tempSensorSelection = (connection, spi) => {
  sendCommand(TEMPERATURE_SENSOR_SELECTION, connection.dcPin, connection.csPin, spi);
  sendData(TEMPERATURE_SENSOR_SELECTION_ARGS.DATA_0.SOURCE.INTERNAL, connection.dcPin, connection.csPin, spi);
};
