# Convert images

This documentation is for the a helpful utility that will let you convert an image (bmp, png, etc) to a format that displays understand.

To see other features of this entire Kaluma-eink package, head back to the main [README](/README.md).

# Example

```bash
npx convert-to-eink -i <file> -w <width> -h <height> > <output>.js
```

# Options

* `-i` (or `--input`): The input image to be converted
* `-w` (or `--width`) [optional]: The output width of the converted image
  * If not specified, the input image width will be used
* `-h` (or `--height`) [optional]: The output height of the converted image
  * If not specified, the input image height will be used

Note: You do not have to specify both `-w` and `-h`, if you only specify one then the image will be scaled with the same aspect ratio as it started with.

# Output

By default the tool will print the converted image output to console. To actually store the converted image output, you must redirect it to a file with `> whatever/file/name/you_want.js`. 

Note: These output image is not compressed, and can easily consume all memory of your mcu. Try to keep the resolution very low and use very few images on your display.

# Example converted file

```javascript
/* eslint-disable max-len */
module.exports = {
  blackCanvas: {
    image: new Uint8Array([255, 255, /* output truncated */]),
    resolutionX: 200,
    resolutionY: 199,
  },
  redCanvas: {
    image: new Uint8Array([255, 255, /* output truncated */]),
    resolutionX: 200,
    resolutionY: 199,
  },
};
```

# Example usage with the EpdBufferedDriver

```javascript
const { GPIO } = require('gpio');
const { SPI } = require('spi');

const {
  spiOptions, epd1in54bv2, EpdBufferedDriver, invert,
} = require('kaluma-eink');

// This is a converted image.
const sillycat = require('./sillycat_200.js');

const connection = {
  csPin: new GPIO(17, OUTPUT),
  dcPin: new GPIO(13, OUTPUT),
  resetPin: new GPIO(12, OUTPUT),
  busyPin: new GPIO(11, INPUT),
};

const spi = new SPI(0, spiOptions);

const driver = new EpdBufferedDriver();
driver.setup(connection, spi, epd1in54bv2);

const gc = driver.getContext();

const RED = gc.color16(255, 0, 0);
const BLACK = gc.color16(0, 0, 0);

gc.drawBitmap(0, 0, {
  width: sillycat.blackCanvas.resolutionX,
  height: sillycat.blackCanvas.resolutionY,
  bpp: 1,
  data: invert(sillycat.blackCanvas.image),
}, { color: BLACK });

gc.drawBitmap(0, 0, {
  width: sillycat.redCanvas.resolutionX,
  height: sillycat.redCanvas.resolutionY,
  bpp: 1,
  data: invert(sillycat.redCanvas.image),
}, { color: RED });

gc.display();

spi.close();
```

# Example usage with the EpdRawDriver

```javascript
const { GPIO } = require('gpio');
const { SPI } = require('spi');

const { spiOptions, epd1in54bv2, EpdRawDriver } = require('kaluma-eink');

// This is a converted image.
const sillycat = require('./sillycat_200.js');

const connection = {
  csPin: new GPIO(17, OUTPUT),
  dcPin: new GPIO(13, OUTPUT),
  resetPin: new GPIO(12, OUTPUT),
  busyPin: new GPIO(11, INPUT),
};

const spi = new SPI(0, spiOptions);

const driver = new EpdRawDriver();
driver.setup(connection, spi, epd1in54bv2);

// Note, this only works because the converted image
// is the exact same resolution as the display. It is
// highly recommended that you use the EpdBufferedDriver.
const data = {
  blackImage: sillycat.blackCanvas.image,
  redImage: sillycat.redCanvas.image,
};

driver.displayData(data);

spi.close();
```

![e-ink display with a picture of a cat and  the text "you like E-ink don't you"](convert_promo_pic.png)



