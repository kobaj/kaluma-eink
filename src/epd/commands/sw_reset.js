const { sendCommand } = require('../chip/transfer.js');
const { untilIdle, untilIdleSync } = require('../chip/wait.js');

const SW_RESET = 0x12;

/**
 * Software reset the display synchronously, this will block.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 */
module.exports.swResetSync = (connection, spi) => {
  sendCommand(SW_RESET, connection.dcPin, connection.csPin, spi);
  untilIdleSync(connection.busyPin);
};

/**
 * Async version of swReset, see swResetSync for documentation.
 */
module.exports.swReset = async (connection, spi) => {
  sendCommand(SW_RESET, connection.dcPin, connection.csPin, spi);
  await untilIdle(connection.busyPin);
};
