const { reset, resetSync } = require('../chip/reset.js');
const { untilIdle, untilIdleSync } = require('../chip/wait.js');

/**
 * Hardware reset the display synchronously, this will block.
 *
 * @param {Object} connection the pins used to talk to the display
 */
module.exports.hwResetSync = (connection) => {
  resetSync(connection.resetPin);
  untilIdleSync(connection.busyPin);
};

/**
 * Async version of hwReset, see hwResetSync for documentation.
 */
module.exports.hwReset = async (connection) => {
  await reset(connection.resetPin);
  await untilIdle(connection.busyPin);
};
