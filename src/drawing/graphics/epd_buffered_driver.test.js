/* eslint-disable no-underscore-dangle */
const chai = require('chai');

const { EpdBufferedDriver } = require('./epd_buffered_driver.js');
const { stubSpi } = require('../../../fixtures/kaluma.js');

const { expect } = chai;

const getConnection = () => ({
  csPin: 4,
  dsPin: 3,
  resetPin: 2,
  busyPin: 1,
});

describe('epd_buffered_driver', () => {
  describe('getContext', () => {
    it('should work fine getting a context for empty epd', () => {
      const driver = new EpdBufferedDriver();
      driver.setup(getConnection(), stubSpi(), { resolutionX: 1, resolutionY: 1 });
      expect(driver.getContext()).to.not.be.null;
    });

    it('should work fine setting and getting pixels', () => {
      const driver = new EpdBufferedDriver();
      driver.setup(getConnection(), stubSpi(), { resolutionX: 2, resolutionY: 2, supportsBlackImage: true });

      driver.setPixel(0, 0, 0); // black
      driver.setPixel(0, 1, 63488); // red
      driver.setPixel(1, 1, 65535); // white

      expect(driver.getPixel(0, 0)).to.equal(0);
      expect(driver.getPixel(0, 1)).to.equal(65535); // different!
      expect(driver.getPixel(1, 1)).to.equal(65535);
    });
  });
});
