const { calcByteIndex } = require('./byte_indexer.js');

/**
 * @returns 0x00 or 0x01 depending on the value found at bit location specified
 *
 * @param {*} bytes the bytes to extract a bit from
 * @param {*} resolutionX the number of bits in a row
 * @param {*} resolutionY the number of rows
 * @param {*} positionX the column of the bit we are interested in
 * @param {*} positionY the row of the bit we are interested in
 */
module.exports.getBit = (bytes, resolutionX, resolutionY, positionX, positionY) => {
  const index = calcByteIndex(resolutionX, resolutionY, positionX, positionY);
  if (index === undefined || bytes.length <= index) {
    return undefined;
  }

  const shiftAmount = (positionX % 8);
  const shift = 0b10000000 >> shiftAmount;
  const byte = bytes[index];
  const bit = (byte & shift) >> (7 - shiftAmount);

  return bit;
};

/**
 * Sets a particular bit inside of bytes to value
 *
 * @param {*} bytes the bytes to write a bit to, this will modify the array
 * @param {*} resolutionX the number of bits in a row
 * @param {*} resolutionY the number of rows
 * @param {*} positionX the column of the bit we are interested in
 * @param {*} positionY the row of the bit we are interested in
 * @param {*} value of either 0x00 or 0x01 that will be set at the specified position
 */
module.exports.setBit = (bytes, resolutionX, resolutionY, positionX, positionY, value) => {
  if (!Number.isSafeInteger(value) || value < 0 || value > 255) {
    throw new Error(`Value ${value} must be an integer between 0 and 255`);
  }

  const index = calcByteIndex(resolutionX, resolutionY, positionX, positionY);
  if (index === undefined || bytes.length <= index) {
    return;
  }

  const shiftAmount = (positionX % 8);
  const shift = 0b10000000 >> shiftAmount;
  const negShift = ~shift;
  const byte = bytes[index] & negShift;
  const bit = (value & 0x01) << (7 - shiftAmount);

  // eslint-disable-next-line no-param-reassign
  bytes[index] = byte | bit;
};
