/* eslint-disable no-empty-function */
/* eslint-disable getter-return */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/no-extraneous-dependencies */
const sinon = require('sinon');
const mock = require('mock-require');

class GPIO {
  get pin() { }
  get mode() { }
  read() { }
  write() { }
  toggle() { }
  low() { }
  high() { }
  irq() { }
}

class SPI {
  transfer() { }
  send() { }
  recv() { }
  close() { }
}

class GraphicsContext {
  getWidth() { }
  getHeight() { }
  clearScreen() { }
  fillScreen() { }
  setRotation() { }
  getRotation() { }
  setColor() { }
  getColor() { }
  setFillColor() { }
  getFillColor() { }
  setFontColor() { }
  getFontColor() { }
  setFont() { }
  setFontScale() { }
  setPixel() { }
  getPixel() { }
  drawLine() { }
  drawRect() { }
  fillRect() { }
  drawCircle() { }
  fillCircle() { }
  drawRoundRect() { }
  fillRoundRect() { }
  drawText() { }
  drawBitmap() { }
}

mock('gpio', { GPIO });
mock('spi', { SPI });
mock('graphics', { GraphicsContext });

module.exports.stubSpi = () => sinon.createStubInstance(SPI);

module.exports.stubGpio = (pin, mode) => {
  const gpio = sinon.createStubInstance(GPIO);
  sinon.stub(gpio, 'pin').get(() => pin);
  sinon.stub(gpio, 'mode').get(() => mode);
  return gpio;
};

module.exports.stubGraphicsContext = () => sinon.createStubInstance(GraphicsContext);
