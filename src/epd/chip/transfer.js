const { SPI } = require('spi');
const { writePin } = require('../accessors/gpio.js');

const chunkArray = function* chunkArray(arr, n) {
  for (let i = 0; i < arr.length; i += n) {
    yield arr.slice(i, i + n);
  }
};

const chunkGenerator = function* chunkGenerator(gen, n) {
  let buffer = [];
  for (const x of gen) {
    if (buffer.length === n) {
      yield buffer;
      buffer = [];
    }

    buffer.push(x);
  }

  if (buffer.length > 0) {
    yield buffer;
  }
};

const buildUint8Arrays = function* buildUint8Arrays(data) {
  if (data instanceof Uint8Array) {
    yield data;
    return;
  }

  const chunkSize = 100;

  if (Array.isArray(data)) {
    for (const chunk of chunkArray(data, chunkSize)) {
      yield new Uint8Array(chunk);
    }
    return;
  }

  if (data.next !== undefined) {
    for (const chunk of chunkGenerator(data, chunkSize)) {
      yield new Uint8Array(chunk);
    }
    return;
  }

  yield new Uint8Array([data]);
};

const spiTransfer = (data, csPin, spi) => {
  writePin(csPin, LOW);

  for (const uint8Data of buildUint8Arrays(data)) {
    const result = spi.send(uint8Data);
    if (result === -1) {
      throw new Error(`Failed to transfer data ${uint8Data}`);
    }
  }

  writePin(csPin, HIGH);
};

/**
 * Send a command to the display.
 *
 * Note: 3-wire spi is not supported.
 *
 * @param {number} command the uint8 command to send
 * @param {(number|Object)} dcPin either an integer or an instance of GPIO
 * @param {(number|Object)} csPin either an integer or an instance of GPIO
 * @param {Object} spi instance of SPI
 */
module.exports.sendCommand = (command, dcPin, csPin, spi) => {
  writePin(dcPin, LOW);
  spiTransfer(command, csPin, spi);
};

/**
 * Send data to the display.
 *
 * Note 3-wire spi is not supported.
 *
 * @param {(number|number[])} data the uint8 data or uint8[] data to send
 * @param {(number|Object)} dcPin either an integer or an instance of GPIO
 * @param {(number|Object)} csPin either an integer or an instance of GPIO
 * @param {Object} spi instance of SPI
 */
module.exports.sendData = (data, dcPin, csPin, spi) => {
  writePin(dcPin, HIGH);
  spiTransfer(data, csPin, spi);
};

/**
 * The SPI options recommended by display manufacturer.
 */
module.exports.spiOptions = {
  mode: SPI.MODE_0,
  baudrate: 2000000,
  bitorder: SPI.MSB,
};
