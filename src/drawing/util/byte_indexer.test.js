const chai = require('chai');

const { calcByteIndex, setByte } = require('./byte_indexer.js');

const { expect } = chai;

describe('byte_indexer', () => {

  describe('calcByteIndex', () => {
    const tests = [
      // Single byte
      { arg: { width: 8, height: 1, positionX: 0, positionY: 0 }, expected: 0 },
      { arg: { width: 4, height: 1, positionX: 0, positionY: 0 }, expected: 0 },
      { arg: { width: 1, height: 1, positionX: 0, positionY: 0 }, expected: 0 },
      { arg: { width: 8, height: 1, positionX: 1, positionY: 0 }, expected: 0 },
      { arg: { width: 8, height: 1, positionX: 4, positionY: 0 }, expected: 0 },
      { arg: { width: 8, height: 1, positionX: 7, positionY: 0 }, expected: 0 },
      // Lots of width
      { arg: { width: 16, height: 1, positionX: 8, positionY: 0 }, expected: 1 },
      { arg: { width: 24, height: 1, positionX: 8, positionY: 0 }, expected: 1 },
      { arg: { width: 24, height: 1, positionX: 16, positionY: 0 }, expected: 2 },
      // Lots of height
      { arg: { width: 8, height: 2, positionX: 0, positionY: 1 }, expected: 1 },
      { arg: { width: 8, height: 3, positionX: 0, positionY: 1 }, expected: 1 },
      { arg: { width: 8, height: 3, positionX: 0, positionY: 2 }, expected: 2 },
      // Grid
      { arg: { width: 24, height: 3, positionX: 0, positionY: 0 }, expected: 0 },
      { arg: { width: 24, height: 3, positionX: 8, positionY: 0 }, expected: 1 },
      { arg: { width: 24, height: 3, positionX: 16, positionY: 0 }, expected: 2 },
      { arg: { width: 24, height: 3, positionX: 0, positionY: 1 }, expected: 3 },
      { arg: { width: 24, height: 3, positionX: 8, positionY: 1 }, expected: 4 },
      { arg: { width: 24, height: 3, positionX: 16, positionY: 1 }, expected: 5 },
      { arg: { width: 24, height: 3, positionX: 0, positionY: 2 }, expected: 6 },
      { arg: { width: 24, height: 3, positionX: 8, positionY: 2 }, expected: 7 },
      { arg: { width: 24, height: 3, positionX: 16, positionY: 2 }, expected: 8 },
      // Undefined
      { arg: { width: 8, height: 1, positionX: -1, positionY: 0 }, expected: undefined },
      { arg: { width: 8, height: 1, positionX: 599, positionY: 0 }, expected: undefined },
      { arg: { width: 8, height: 1, positionX: 0, positionY: -1 }, expected: undefined },
      { arg: { width: 8, height: 1, positionX: 0, positionY: 999 }, expected: undefined },
      { arg: { width: 8, height: 1, positionX: 8, positionY: 0 }, expected: undefined },
      { arg: { width: 8, height: 1, positionX: 0, positionY: 1 }, expected: undefined },
    ];

    tests.forEach(({ arg, expected }) => {
      const { width, height, positionX, positionY } = arg;
      it(`should get the index of ${JSON.stringify(arg)}`, async () => {
        expect(calcByteIndex(width, height, positionX, positionY)).to.equal(expected);
      });
    });
  });

  describe('setByte', () => {

    const tests = [
      { arg: { bytes: [0x00], width: 8, height: 1, positionX: 0, positionY: 0, value: 0xFF }, expected: [0xFF] },
      { arg: { bytes: [0x00, 0x00, 0x00], width: 24, height: 1, positionX: 8, positionY: 0, value: 0xFF }, expected: [0x00, 0xFF, 0x00] },
      { arg: { bytes: [0x00, 0x00, 0x00], width: 8, height: 3, positionX: 0, positionY: 1, value: 0xFF }, expected: [0x00, 0xFF, 0x00] },
    ];

    tests.forEach(({ arg, expected }) => {
      const { bytes, width, height, positionX, positionY, value } = arg;
      it(`should set a byte for ${JSON.stringify(arg)}`, async () => {
        setByte(bytes, width, height, positionX, positionY, value);
        expect(bytes).to.deep.equal(expected);
      });
    });

    const negativeTests = [
      // Non byte aligned positions
      { arg: { width: 8, height: 1, positionX: 1, positionY: 0 } },
      { arg: { width: 8, height: 1, positionX: 2, positionY: 0 } },
      { arg: { width: 8, height: 1, positionX: 3, positionY: 0 } },
      { arg: { width: 8, height: 1, positionX: 4, positionY: 0 } },
      { arg: { width: 8, height: 1, positionX: 5, positionY: 0 } },
      { arg: { width: 8, height: 1, positionX: 6, positionY: 0 } },
      { arg: { width: 8, height: 1, positionX: 7, positionY: 0 } },
      { arg: { width: 16, height: 1, positionX: 9, positionY: 0 } },
      { arg: { width: 16, height: 1, positionX: 10, positionY: 0 } },
      { arg: { width: 16, height: 1, positionX: 11, positionY: 0 } },
      { arg: { width: 16, height: 1, positionX: 12, positionY: 0 } },
      { arg: { width: 16, height: 1, positionX: 13, positionY: 0 } },
      { arg: { width: 16, height: 1, positionX: 14, positionY: 0 } },
      { arg: { width: 16, height: 1, positionX: 15, positionY: 0 } },
    ];

    negativeTests.forEach(({ arg }) => {
      const { width, height, positionX, positionY } = arg;
      it(`should not set a byte for ${JSON.stringify(arg)}`, async () => {
        expect(() => setByte([], width, height, positionX, positionY, 0x00)).to.throw();
      });
    });
  });
});
