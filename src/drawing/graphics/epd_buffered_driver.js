/* eslint-disable no-restricted-syntax */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
const { GraphicsContext } = require('graphics');

const { create, FILL } = require('../util/raw_image.js');
const { getBit, setBit } = require('../util/bit_accessor.js');
const { fillRect } = require('../util/fill_rect.js');
const { invert } = require('../util/invert.js');

const { apply } = require('../context/context_state_remover.js');

const { displayFrame, displayFrameSync } = require('../../epd/epd_generic.js');

const BLACK_PIXEL = 0;
const RED_PIXEL = 63488;
const WHITE_PIXEL = 65535;

module.exports.EpdBufferedDriver = class {
  constructor() {
    this.display = this.display.bind(this);
    this.displayAsync = this.displayAsync.bind(this);
    this.getPixel = this.getPixel.bind(this);
    this.setPixel = this.setPixel.bind(this);
    this.clearScreen = this.clearScreen.bind(this);
  }

  setup(connection, spi, epd) {
    this.connection = connection;
    this.spi = spi;
    this.epd = epd;

    if (epd === undefined) {
      throw new Error('EPD must be defined.');
    }

    if (connection === undefined || spi === undefined) {
      throw new Error('Connection and SPI need to be defined.');
    }

    this.clearScreen();
  }

  getContext() {
    if (this.context) {
      return this.context;
    }

    // We deliberately do not use a BufferedGraphicsContext
    // because we maintin the buffer ourselves instead.
    this.context = apply(new GraphicsContext(this.epd.resolutionX, this.epd.resolutionY, {
      rotation: 0, // 1 means 90 degrees
      setPixel: this.setPixel,
      getPixel: this.getPixel,
      fillRect: (posX, posY, width, height, color) => {
        const redBit = color === RED_PIXEL ? FILL.ZERO : FILL.ONE;
        const blackBit = color === BLACK_PIXEL ? FILL.ZERO : FILL.ONE;

        fillRect(this.redImage, this.epd.resolutionX, this.epd.resolutionY, posX, posY, width, height, redBit);
        fillRect(this.blackImage, this.epd.resolutionX, this.epd.resolutionY, posX, posY, width, height, blackBit);
      },
    }));

    this.context.display = this.display;
    this.context.displaySync = this.displaySync;
    this.context.clearScreen = this.clearScreen;
    return this.context;
  }

  display() {
    // No need to clear the display, since this class will initialize the entire ram anyway.
    displayFrameSync(this.connection, this.spi, { blackImage: this.blackImage, redImage: this.redImage }, { clear: false }, this.epd);
  }

  async displayAsync() {
    await displayFrame(this.connection, this.spi, { blackImage: this.blackImage, redImage: this.redImage }, { clear: false }, this.epd);
  }

  getPixel(x, y) {
    const redBit = getBit(this.redImage, this.epd.resolutionX, this.epd.resolutionY, x, y);
    const blackBit = getBit(this.blackImage, this.epd.resolutionX, this.epd.resolutionY, x, y);

    if (redBit === FILL.ZERO) {
      return RED_PIXEL;
    }

    if (blackBit === FILL.ZERO) {
      return BLACK_PIXEL;
    }

    return WHITE_PIXEL;
  }

  setPixel(x, y, color) {
    const redBit = color === RED_PIXEL ? FILL.ZERO : FILL.ONE;
    const blackBit = color === BLACK_PIXEL ? FILL.ZERO : FILL.ONE;

    setBit(this.redImage, this.epd.resolutionX, this.epd.resolutionY, x, y, redBit);
    setBit(this.blackImage, this.epd.resolutionX, this.epd.resolutionY, x, y, blackBit);
  }

  drawBitmap(x, y, bitmap) {
    const gc = this.getContext();

    if (this.epd.supportsRedImage) {
      gc.drawBitmap(x, y, {
        width: bitmap.redCanvas.resolutionX,
        height: bitmap.redCanvas.resolutionY,
        bpp: 1,
        data: invert(bitmap.redCanvas.image),
      }, { color: RED_PIXEL });
    }

    if (this.epd.supportsBlackImage) {
      gc.drawBitmap(0, 0, {
        width: bitmap.blackCanvas.resolutionX,
        height: bitmap.blackCanvas.resolutionY,
        bpp: 1,
        data: invert(bitmap.blackCanvas.image),
      }, { color: BLACK_PIXEL });
    }
  }

  clearScreen() {
    this.redImage = this.epd.supportsRedImage ? create(this.epd.resolutionX, this.epd.resolutionY, FILL.ONE) : [];
    this.blackImage = this.epd.supportsBlackImage ? create(this.epd.resolutionX, this.epd.resolutionY, FILL.ONE) : [];
  }
};
