const { sendCommand, sendData } = require('../chip/transfer.js');

const DATA_ENTRY_MODE = 0x11;

const DATA_ENTRY_MODE_ARGS = {
  DATA_0: {
    ADDRESS_DIRECTION: {
      X: 0b000,
      Y: 0b100,
    },
    ADDRESS_INCREMENT: {
      Y_DEC_X_DEC: 0b00,
      Y_DEC_X_INC: 0b01,
      Y_INC_X_DEC: 0b10,
      Y_INC_X_INC: 0b11,
    },
  },
};

/**
 * Set how data is entered into display ram.
 *
 * By default this will set the address direction to be X.
 * By default this will set the address increment to Y increasing and X increasing.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 */
module.exports.dataEntryMode = (connection, spi) => {
  sendCommand(DATA_ENTRY_MODE, connection.dcPin, connection.csPin, spi);
  const dataArgs0 = DATA_ENTRY_MODE_ARGS.DATA_0;
  const dataEntrySettings = dataArgs0.ADDRESS_DIRECTION.X | dataArgs0.ADDRESS_INCREMENT.Y_INC_X_INC;
  sendData(dataEntrySettings, connection.dcPin, connection.csPin, spi);
};
