const { GPIO } = require('gpio');

const isKaluma = (pin) => {
  if (Number.isInteger(pin)) {
    return false;
  }

  if (pin instanceof GPIO) {
    return true;
  }

  throw new Error(`Pin ${pin} needs to either be an integer, or an instance of GPIO.`);
};

/**
 * Similar to digitalWrite, but works for multiple types of pins.
 *
 * @param {(number|Object)} pin either an integer or an instance of GPIO
 * @param {(HIGH|LOW)} value either HIGH or LOW
 */
module.exports.writePin = (pin, value) => {
  if (value !== HIGH && value !== LOW) {
    throw new Error(`Value ${value} needs to be either HIGH (1) or LOW (0) when writing.`);
  }

  if (!isKaluma(pin)) {
    // We'll just hope the user specified this was an output pin before hand!
    // eslint-disable-next-line no-restricted-syntax
    digitalWrite(pin, value);
    return;
  }

  if (pin.mode !== OUTPUT) {
    throw new Error(`Pin ${pin.pin} needs to be OUTPUT in order to write data, pin is currently ${pin.mode}.`);
  }

  pin.write(value);
};

/**
 * Similar to digitalRead, but works for multiple types of pins.
 *
 * @param {(number|Object)} pin either an integer or an instance of GPIO
 * @returns HIGH or LOW
 */
module.exports.readPin = (pin) => {
  if (!isKaluma(pin)) {
    // We'll just hope the user specified this was an input pin before hand!
    // eslint-disable-next-line no-restricted-syntax
    return digitalRead(pin);
  }

  if (pin.mode !== INPUT && pin.mode !== INPUT_PULLUP && pin.mode !== INPUT_PULLDOWN) {
    throw new Error(`Pin ${pin.pin} needs to be INPUT, INPUT_PULLUP, or INPUT_PULLDOWN in order to read data, pin is currently ${pin.mode}.`);
  }

  return pin.read();
};
