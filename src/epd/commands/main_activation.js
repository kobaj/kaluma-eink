const { sendCommand } = require('../chip/transfer.js');
const { untilIdle, untilIdleSync } = require('../chip/wait.js');

const MAIN_ACTIVATION = 0x20;

const TIMEOUT = 1000 * 60; // can take up to a full minute!

/**
 * Trigger a display refresh which will change the e-ink to display
 * the contents written to ram synchronously, this will block.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 */
module.exports.mainActivationSync = (connection, spi) => {
  sendCommand(MAIN_ACTIVATION, connection.dcPin, connection.csPin, spi);
  untilIdleSync(connection.busyPin, TIMEOUT);
};

/**
 * Trigger a display refresh which will change the e-ink to display the contents written to ram.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 */
module.exports.mainActivation = async (connection, spi) => {
  sendCommand(MAIN_ACTIVATION, connection.dcPin, connection.csPin, spi);
  await untilIdle(connection.busyPin, TIMEOUT);
};
