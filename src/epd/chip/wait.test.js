const chai = require('chai');

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const chaiAsPromised = require('chai-as-promised');

const { stubGpio } = require('../../../fixtures/kaluma.js');
const { counter, counterReset } = require('../../../fixtures/next_call.js');

const { untilIdle, untilIdleSync } = require('./wait.js');

chai.use(chaiAsPromised);
chai.use(sinonChai);
const { expect } = chai;

describe('wait', () => {
  const gpioInputPin = stubGpio(1, INPUT);
  const tCount = counter();

  beforeEach('reset', () => {
    sinon.reset();
    counterReset();
  });

  describe('untilIdle', () => {
    it('should succeed within time', async () => {
      gpioInputPin.read.onCall(tCount()).returns(HIGH);
      gpioInputPin.read.onCall(tCount()).returns(HIGH);
      gpioInputPin.read.onCall(tCount()).returns(LOW);

      await untilIdle(gpioInputPin, 500);

      expect(gpioInputPin.read).callCount(3);
    });

    it('should timeout', async () => {
      gpioInputPin.read.returns(HIGH);

      await expect(untilIdle(gpioInputPin, 500)).to.eventually.rejected;
      expect(gpioInputPin.read).callCount(52);
    });
  });

  describe('untilIdleSync', () => {
    it('should succeed within time', async () => {
      gpioInputPin.read.onCall(tCount()).returns(HIGH);
      gpioInputPin.read.onCall(tCount()).returns(HIGH);
      gpioInputPin.read.onCall(tCount()).returns(LOW);

      untilIdleSync(gpioInputPin, 500);

      expect(gpioInputPin.read).callCount(3);
    });

    it('should timeout', async () => {
      gpioInputPin.read.returns(HIGH);

      expect(() => untilIdleSync(gpioInputPin, 500)).to.throw();
      expect(gpioInputPin.read).callCount(52);
    });
  });
});
