module.exports.validateFreeMemory = (neededMemBytes) => {
  // A really bad estimate, but better than nothing.

  const memUsage = process.memoryUsage();
  const availableMemBytes = memUsage.heapTotal - memUsage.heapUsed;
  if (neededMemBytes > availableMemBytes) {
    throw new Error(`Cannot allocate new Uint8Array, not enough memory. Available: ${availableMemBytes}, Needed: ${neededMemBytes}`);
  }
};
