const chai = require('chai');

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const chaiAsPromised = require('chai-as-promised');

const { stubSpi, stubGpio } = require('../../fixtures/kaluma.js');
const { stubSetTimeout, restoreClock } = require('../../fixtures/fake_clock.js');

const { displayFrame, displayFrameSync } = require('./epd_generic.js');

chai.use(chaiAsPromised);
chai.use(sinonChai);
const { expect } = chai;

const assertNormalFlow = (connection, spi) => {
  expect(connection.resetPin.write.getCall(0)).to.be.calledWith(HIGH); // HW reset
  expect(connection.resetPin.write.getCall(1)).to.be.calledWith(LOW); // HW reset
  expect(connection.resetPin.write.getCall(2)).to.be.calledWith(HIGH); // HW reset
  expect(spi.send.getCall(0)).to.be.calledWith(new Uint8Array([0x12])); // SW reset
  expect(spi.send.getCall(1)).to.be.calledWith(new Uint8Array([0x01])); // driver output control
  expect(spi.send.getCall(5)).to.be.calledWith(new Uint8Array([0x11])); // data entry mode
  expect(spi.send.getCall(7)).to.be.calledWith(new Uint8Array([0x3C])); // border waveform control
  expect(spi.send.getCall(9)).to.be.calledWith(new Uint8Array([0x44])); // set ram address
  expect(spi.send.getCall(12)).to.be.calledWith(new Uint8Array([0x45])); // set ram address
  expect(spi.send.getCall(17)).to.be.calledWith(new Uint8Array([0x4E])); // set ram address pointer
  expect(spi.send.getCall(19)).to.be.calledWith(new Uint8Array([0x4F])); // set ram address pointer
  expect(spi.send.getCall(22)).to.be.calledWith(new Uint8Array([0x22])); // display update control
  expect(spi.send.getCall(24)).to.be.calledWith(new Uint8Array([0x21])); // display update control
  expect(spi.send.getCall(27)).to.be.calledWith(new Uint8Array([0x18])); // set temperature source
  // Data Writing would go here
  expect(spi.send.getCall(29)).to.be.calledWith(new Uint8Array([0x10])); // deep sleep mode
};

const assertDataWriting = (connection, spi) => {
  expect(spi.send.getCall(29)).to.be.calledWith(new Uint8Array([0x4E])); // set ram address pointer
  expect(spi.send.getCall(31)).to.be.calledWith(new Uint8Array([0x4F])); // set ram address pointer
  expect(spi.send.getCall(34)).to.be.calledWith(new Uint8Array([0x24])); // write ram (clear)
  // Multiple calls to write data, this number is dependent on resolution and the number of chunks.
  const dataWriteCalls = 56;
  expect(spi.send.getCall(36 + dataWriteCalls)).to.be.calledWith(new Uint8Array([0x4E])); // set ram address pointer
  expect(spi.send.getCall(38 + dataWriteCalls)).to.be.calledWith(new Uint8Array([0x4F])); // set ram address pointer
  expect(spi.send.getCall(41 + dataWriteCalls)).to.be.calledWith(new Uint8Array([0x24])); // write ram (data)
  expect(spi.send.getCall(43 + dataWriteCalls)).to.be.calledWith(new Uint8Array([0x20])); // main activation (data)
  expect(spi.send.getCall(44 + dataWriteCalls)).to.be.calledWith(new Uint8Array([0x10])); // deep sleep mode
};

const assertFailureResolution = (connection, spi) => {
  expect(connection.resetPin.write.getCall(3)).to.be.calledWith(HIGH); // HW reset
  expect(connection.resetPin.write.getCall(4)).to.be.calledWith(LOW); // HW reset
  expect(connection.resetPin.write.getCall(5)).to.be.calledWith(HIGH); // HW reset

  const calls = spi.send.getCalls(); // The "Last" call must be sleep.
  expect(spi.send.getCall(calls.length - 2)).to.be.calledWith(new Uint8Array([0x10])); // deep sleep mode
};

describe('epd_generic', () => {
  const display = {
    startX: 0,
    resolutionX: 152,
    startY: 0,
    resolutionY: 296,
    supportsBlackImage: true,
  };
  const connection = {
    csPin: stubGpio(1, OUTPUT),
    dcPin: stubGpio(1, OUTPUT),
    resetPin: stubGpio(1, OUTPUT),
    busyPin: stubGpio(1, INPUT),
  };
  const spi = stubSpi();

  beforeEach('reset', () => {
    sinon.reset();
    stubSetTimeout();
  });

  afterEach('clear', () => {
    restoreClock();
  });

  describe('displayFrame', () => {
    it('should succeed through all the normal flow operations', async () => {
      await displayFrame(connection, spi, /* data= */ undefined, /* options= */ undefined, display);

      assertNormalFlow(connection, spi);
    });

    it('should clear the display and write data if they are set', async () => {
      await displayFrame(connection, spi, { blackImage: [0xFF] }, { clear: true }, display);

      assertDataWriting(connection, spi);
    });

    it('should turn off display even upon failure', async () => {
      spi.send.withArgs(new Uint8Array([0x24])).throws();

      await expect(displayFrame(connection, spi, { blackImage: [0xFF] }, { clear: true }, display)).to.eventually.be.rejected;

      assertFailureResolution(connection, spi);
    });
  });

  describe('displayFrameSync', () => {
    it('should succeed through all the normal flow operations', async () => {
      displayFrameSync(connection, spi, /* data= */ undefined, /* options= */ undefined, display);

      assertNormalFlow(connection, spi);
    });

    it('should clear the display and write data if they are set', async () => {
      displayFrameSync(connection, spi, { blackImage: [0xFF] }, { clear: true }, display);

      assertDataWriting(connection, spi);
    });

    it('should turn off display even upon failure', async () => {
      spi.send.withArgs(new Uint8Array([0x24])).throws();

      expect(() => displayFrameSync(connection, spi, { blackImage: [0xFF] }, { clear: true }, display)).to.throw();

      assertFailureResolution(connection, spi);
    });
  });
});
