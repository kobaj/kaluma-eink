#!/usr/bin/env node

const { parseArgs } = require('node:util');
const { convert } = require('./convert.js');

const options = {
  input: { type: 'string', short: 'i' },
  width: { type: 'string', short: 'w' },
  height: { type: 'string', short: 'h' },
};
const { values } = parseArgs({ options, tokens: true });
const { input, width, height } = values;

const parseDimension = (value) => {
  const partial = parseInt(value, /* radix= */ 10);
  if (Number.isNaN(partial) || partial <= 0) {
    return 0;
  }

  return partial;
};

const run = async () => {
  const parsedWidth = parseDimension(width);
  const parsedHeight = parseDimension(height);

  const printablePainting = await convert(input, parsedWidth, parsedHeight);

  // eslint-disable-next-line no-console
  console.log(printablePainting);
};

run();
