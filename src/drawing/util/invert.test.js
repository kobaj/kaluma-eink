const chai = require('chai');

const { invert } = require('./invert.js');

const { expect } = chai;

describe('invert', () => {

  const tests = [
    { arg: { image: [], resolutionX: 8, resolutionY: 7 }, expected: { image: new Uint8Array([]), resolutionX: 8, resolutionY: 7 } },
    { arg: { image: [0x00], resolutionX: 8, resolutionY: 7 }, expected: { image: new Uint8Array([0xFF]), resolutionX: 8, resolutionY: 7 } },
    { arg: { image: [0xFF], resolutionX: 8, resolutionY: 7 }, expected: { image: new Uint8Array([0x00]), resolutionX: 8, resolutionY: 7 } },
    { arg: { image: [0xFF, 0x00, 0xFF], resolutionX: 8, resolutionY: 7 }, expected: { image: new Uint8Array([0x00, 0xFF, 0x00]), resolutionX: 8, resolutionY: 7 } },
    /* eslint-disable-next-line max-len */
    { arg: { image: [0x00, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x00, 0x10, 0x00, 0x00], resolutionX: 8, resolutionY: 7 }, expected: { image: new Uint8Array([0xFF, 0xEF, 0xEF, 0xEF, 0xEF, 0xEF, 0xEF, 0xEF, 0xFF, 0xEF, 0xFF, 0xFF]), resolutionX: 8, resolutionY: 7 } },
  ];

  tests.forEach(({ arg, expected }) => {
    it(`should invert ${JSON.stringify(arg)}`, async () => {
      const canvas = invert(arg.image);

      expect(canvas).to.deep.equal(expected.image);
    });
  });
});
