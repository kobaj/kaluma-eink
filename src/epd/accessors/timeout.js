/**
 * Delay execution asynchronously.
 *
 * @param {number} ms how long to timeout/sleep/delay
 * @returns a Promise to be awaited on
 */
module.exports.timeout = async (ms) => new Promise((resolve) => { setTimeout(resolve, ms); });
