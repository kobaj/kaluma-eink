const chai = require('chai');

const { bytePrinter } = require('../../../fixtures/print.js');

const { create } = require('./raw_image.js');

chai.use(bytePrinter);

const { expect } = chai;

describe('raw_image', () => {
  const tests = [
    { arg: { width: 0, height: 0, fill: 0x00 }, expected: { image: new Uint8Array([]) } },
    { arg: { width: 1, height: 0, fill: 0x00 }, expected: { image: new Uint8Array([]) } },
    { arg: { width: 0, height: 1, fill: 0x00 }, expected: { image: new Uint8Array([]) } },
    { arg: { width: 1, height: 1, fill: 0x00 }, expected: { image: new Uint8Array([0x00]) } },
    { arg: { width: 1, height: 2, fill: 0x00 }, expected: { image: new Uint8Array([0x00, 0x00]) } },
    { arg: { width: 5, height: 1, fill: 0x00 }, expected: { image: new Uint8Array([0x00]) } },
    { arg: { width: 8, height: 1, fill: 0x00 }, expected: { image: new Uint8Array([0x00]) } },
    { arg: { width: 8, height: 2, fill: 0x00 }, expected: { image: new Uint8Array([0x00, 0x00]) } },
    { arg: { width: 1.5, height: 1, fill: 0x00 }, expected: { image: new Uint8Array([0x00]) } },
    { arg: { width: 1, height: 1.5, fill: 0x00 }, expected: { image: new Uint8Array([0x00, 0x00]) } },
    { arg: { width: 8.5, height: 1, fill: 0x00 }, expected: { image: new Uint8Array([0x00, 0x00]) } },
  ];

  tests.forEach(({ arg, expected }) => {
    it(`should create a raw image for ${JSON.stringify(arg)}`, async () => {
      const result = create(arg.width, arg.height, arg.fill);
      expect(result).to.deep.equal(expected.image);
    });
  });
});
