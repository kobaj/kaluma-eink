/**
 * @returns the most significant 8 bits of the address
 *
 * For example, an input of 0b0000000011111111 will return 0b11111111.
 */
module.exports.getMostSignificantBits = (address) => address & 0xFF;

/**
 * @returns the least significant 8 bits of the address
 *
 * For example, an input of 0b0000000011111111 will return 0b00000000.
 */
module.exports.getLeastSignificantBits = (address) => (address >> 8) & 0xFF;
