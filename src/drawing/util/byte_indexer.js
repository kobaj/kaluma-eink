/**
 * @returns the index of the byte at positionX, positionY of a given buffer with resolutionX and resolutionY
 *
 * Example:
 * resolutionX = 16 resolutionY = 3 positionX = 12 positionY = 1
 * result = 3
 *
 * @param {*} resolutionX the number of bits in a row
 * @param {*} resolutionY the number of rows
 * @param {*} positionX the column of the bit we are interested in
 * @param {*} positionY the row of the bit we are interested in
 */
module.exports.calcByteIndex = (resolutionX, resolutionY, positionX, positionY) => {
  if (positionX < 0 || positionX >= resolutionX) {
    return undefined;
  }

  if (positionY < 0 || positionY >= resolutionY) {
    return undefined;
  }

  const indexX = Math.floor(positionX / 8);
  const alignedresolutionX = Math.ceil(resolutionX / 8);
  const index = positionY * alignedresolutionX + indexX;

  return index;
};

/**
 * Sets a particular byte inside of bytes to value
 *
 * Note: If the position is not byte alligned, this will throw an exception.
 *
 * @param {*} bytes the bytes to write a bit to, this will modify the array
 * @param {*} resolutionX the number of bits in a row
 * @param {*} resolutionY the number of rows
 * @param {*} positionX the column of the bit we are interested in
 * @param {*} positionY the row of the bit we are interested in
 * @param {*} value the full byte that will be set at the specified position
 */
module.exports.setByte = (bytes, resolutionX, resolutionY, positionX, positionY, value) => {
  if (!Number.isSafeInteger(value) || value < 0 || value > 255) {
    throw new Error(`Value ${value} must be an integer between 0 and 255`);
  }

  if (positionX % 8 !== 0) {
    throw new Error(`PositionX ${positionX} is not byte aligned.`);
  }

  const index = this.calcByteIndex(resolutionX, resolutionY, positionX, positionY);
  if (index === undefined) {
    return;
  }

  // eslint-disable-next-line no-param-reassign
  bytes[index] = value;
};

/**
 * @returns the byte found at the specified position
 *
 * Note: If the position is not byte alligned, this will throw an exception.
 *
 * @param {*} bytes the bytes to write a bit to, this will modify the array
 * @param {*} resolutionX the number of bits in a row
 * @param {*} resolutionY the number of rows
 * @param {*} positionX the column of the bit we are interested in
 * @param {*} positionY the row of the bit we are interested in
 */
module.exports.getByte = (bytes, resolutionX, resolutionY, positionX, positionY) => {
  if (positionX % 8 !== 0) {
    throw new Error(`PositionX ${positionX} is not byte aligned.`);
  }

  const index = this.calcByteIndex(resolutionX, resolutionY, positionX, positionY);
  if (index === undefined) {
    return undefined;
  }

  return bytes[index];
};
