module.exports = {
  startX: 0,
  resolutionX: 200,
  startY: 0,
  resolutionY: 200,
  supportsBlackImage: true,
  supportsRedImage: true,
};
