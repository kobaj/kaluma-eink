const chai = require('chai');

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const { stubSpi, stubGpio } = require('../../../fixtures/kaluma.js');
const { counter, counterReset } = require('../../../fixtures/next_call.js');

const { borderWaveformControl } = require('./border_waveform_control.js');

chai.use(sinonChai);
const { expect } = chai;

describe('border_waveform_control', () => {
  const connection = {
    csPin: stubGpio(1, OUTPUT),
    dcPin: stubGpio(1, OUTPUT),
    resetPin: stubGpio(1, OUTPUT),
    busyPin: stubGpio(1, INPUT),
  };
  const spi = stubSpi();
  const tCount = counter();

  beforeEach('reset', () => {
    sinon.reset();
    counterReset();
  });

  describe('call', () => {
    it('should succeed with display of 296', async () => {
      borderWaveformControl(connection, spi);

      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x3C]));
      expect(spi.send.getCall(tCount())).calledWith(new Uint8Array([0x05]));
    });
  });
});
