/* eslint-disable global-require */
module.exports = {
  // SPI
  spiOptions: require('./src/epd/chip/transfer.js').spiOptions,
  // Displays
  epd1in54bv2: require('./src/epd_1in54b_v2.js'),
  epd2in66: require('./src/epd_2in66.js'),
  // Graphics
  ...require('./src/drawing/graphics/epd_buffered_driver.js'),
  ...require('./src/drawing/graphics/epd_raw_driver.js'),
  // utilites
  ...require('./src/drawing/util/invert.js'),

};
