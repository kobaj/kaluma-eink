let index = 0;
const counters = {};

module.exports.counter = () => {
  const myIndex = index;
  index += 1;
  counters[myIndex] = 0;

  return () => {
    const value = counters[myIndex];
    counters[myIndex] += 1;
    return value;
  };
};

module.exports.counterReset = () => {
  Object.keys(counters).forEach((key) => { counters[key] = 0; });
};
