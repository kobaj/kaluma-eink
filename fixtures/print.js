module.exports.printByte = (b) => b.toString(2).padStart(8, '0');

module.exports.printBytes = (bytes) => [...bytes].map(module.exports.printByte);

module.exports.bytePrinter = (_chai, util) => {
  const looksLikeAUint8Array = (obj) => {

    if (!(obj instanceof Array)) {
      return false;
    }

    for (const n of obj) {
      if (!Number.isSafeInteger(n)) {
        return false;
      }

      if (n < 0) {
        return false;
      }

      if (n > 0xFF) {
        return false;
      }
    }

    return true;
  };

  const objDisplay = (obj) => {
    if (obj instanceof Uint8Array) {
      return `Uint8Array[${module.exports.printBytes(obj)}]`;
    }

    if (looksLikeAUint8Array(obj)) {
      return `[${module.exports.printBytes(obj)}]`;
    }

    return util.objDisplay(obj);
  };

  // Ideally we only override objDisplay.
  // but for some reason that doesn't work.
  // So we have to override one level up, which is getMessage.

  // eslint-disable-next-line no-param-reassign
  _chai.util.getMessage = (obj, args) => {
    const negate = util.flag(obj, 'negate');
    const val = util.flag(obj, 'object');
    const expected = args[3];
    const actual = util.getActual(obj, args);
    let msg = negate ? args[2] : args[1];
    const flagMsg = util.flag(obj, 'message');

    if (typeof msg === 'function') {
      msg = msg();
    }
    msg = msg || '';
    msg = msg
      .replace(/#\{this\}/g, () => objDisplay(val))
      .replace(/#\{act\}/g, () => objDisplay(actual))
      .replace(/#\{exp\}/g, () => objDisplay(expected));

    return flagMsg ? `${flagMsg}: ${msg}` : msg;
  };
};
