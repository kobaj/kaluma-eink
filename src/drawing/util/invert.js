const { toUint8Array } = require('./to_uint8array.js');

/**
 * @returns the byte array every byte inverted
 *
 * Example:
 * bytes = [0b00001010]
 * output = [0b11110101]
 *
 * @param {*} image the input byte array
 */
module.exports.invert = (image) => toUint8Array(image).map((b) => ~b);
