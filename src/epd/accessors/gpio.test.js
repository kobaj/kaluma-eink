/* eslint-disable no-restricted-syntax */
const chai = require('chai');

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const { stubGpio } = require('../../../fixtures/kaluma.js');

const { writePin, readPin } = require('./gpio.js');

chai.use(sinonChai);
const { expect } = chai;

describe('gpio', () => {
  const gpioOutputPin = stubGpio(1, OUTPUT);
  const gpioInputPin = stubGpio(1, INPUT);

  beforeEach('reset', () => {
    sinon.reset();
  });

  describe('writePin', () => {
    it('should fail with undefined pin', async () => {
      expect(() => writePin(undefined, HIGH)).to.throw();
    });

    it('should fail with unknown pin', async () => {
      expect(() => writePin('hi', HIGH)).to.throw();
    });

    it('should fail with input pin', async () => {
      expect(() => writePin(gpioInputPin, HIGH)).to.throw();
    });

    it('should fail with unknown value on gpio', async () => {
      expect(() => writePin(gpioOutputPin, 2)).to.throw();
    });

    it('should fail with unknown value on pin', async () => {
      expect(() => writePin(1, 2)).to.throw();
    });

    it('should succeed with gpio', async () => {
      writePin(gpioOutputPin, HIGH);

      expect(gpioOutputPin.write).calledWith(HIGH);
    });

    it('should succeed with pin', async () => {
      writePin(1, HIGH);

      expect(digitalWrite).calledWith(1, HIGH);
    });
  });

  describe('readPin', () => {
    it('should fail with undefined pin', async () => {
      expect(() => readPin(undefined, HIGH)).to.throw();
    });

    it('should fail with unknown pin', async () => {
      expect(() => readPin('hi', HIGH)).to.throw();
    });

    it('should fail with output pin', async () => {
      expect(() => readPin(gpioOutputPin, HIGH)).to.throw();
    });

    it('should fail with unknown value', async () => {
      expect(() => readPin(undefined, 2)).to.throw();
    });

    it('should succeed with gpio', async () => {
      gpioInputPin.read.returns(HIGH);

      const value = readPin(gpioInputPin, HIGH);

      expect(gpioInputPin.read).calledWith();
      expect(value).to.equal(HIGH);
    });

    it('should succeed with pin', async () => {
      digitalRead.returns(HIGH);

      const value = readPin(1);

      expect(digitalRead).calledWith(1);
      expect(value).to.equal(HIGH);
    });
  });
});
