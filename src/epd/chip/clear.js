const validateBound = (name, bound) => {
  if (bound === undefined || !Number.isInteger(bound) || bound <= 0) {
    throw new Error(`${name} ${bound} needs to be a positive integer greater than zero.`);
  }
};

const validateOnlyOne = (nameA, boundA, nameB, boundB) => {
  if (boundA !== undefined && boundB !== undefined) {
    throw new Error(`Only one of ${nameA} ${boundA} or ${nameB} ${boundB} should be set.`);
  }

  if (boundA === undefined && boundB === undefined) {
    throw new Error(`One of ${nameA} or ${nameB} should be set.`);
  }
};

const clearGenerator = function* clearGenerator(length) {
  for (let i = 0; i < length; i += 1) {
    yield 0xFF;
  }
};

const buildClearData = (resolutionX, resolutionY, display) => {
  validateBound('resolutionX', resolutionX);
  validateBound('resolutionY', resolutionY);

  const width = Math.ceil(resolutionX / 8);
  const height = resolutionY;
  return {
    blackImage: display.supportsBlackImage ? clearGenerator(width * height) : undefined,
    redImage: display.supportsRedImage ? clearGenerator(width * height) : undefined,
  };
};

/**
 * Generate data that will clear the display (set all the pixels to white).
 *
 * @param {Object} display properties about the display including its resolution and supported colors
 * @returns an object containing blackImage and redImage data that will clear the display (set all pixels to white)
 */
module.exports.buildClearData = (display) => {
  validateOnlyOne('resolutionX', display.resolutionX, 'endX', display.endX);
  validateOnlyOne('resolutionY', display.resolutionY, 'endY', display.endY);

  const resolutionX = display.resolutionX ?? ((display.endX - display.startX + 1) * 8);
  const resolutionY = display.resolutionY ?? (display.endY - display.startY + 1);

  return buildClearData(resolutionX, resolutionY, display);
};
