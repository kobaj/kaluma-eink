const { sendCommand, sendData } = require('../chip/transfer.js');

const BORDER_WAVEFORM_CONTROL = 0x3C;

const BORDER_WAVEFORM_CONTROL_ARGS = {
  DATA_0: {
    SELECT_VBD: {
      GS_TRANSITION: 0b00000000,
      FIX_LEVEL: 0b01000000,
      VCOM: 0b10000000,
      HIZ: 0b11000000,
    },
    VBD_LEVEL: {
      VSS: 0b000000,
      VSH1: 0b010000,
      VSL: 0b100000,
      VSH2: 0b110000,
    },
    GS_TRANSITION_CONTROL: {
      FOLLOW_LUT_VCOM_AT_RED: 0b000,
      FOLLOW_LUT: 0b100,
    },
    VBD_TRANSITION: {
      LUT0: 0b00,
      LUT1: 0b01,
      LUT2: 0b10,
      LUT3: 0b11,
    },
  },
};

/**
 * Sets the border on the display.
 *
 * By default this will set a white border.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 */
module.exports.borderWaveformControl = (connection, spi) => {
  sendCommand(BORDER_WAVEFORM_CONTROL, connection.dcPin, connection.csPin, spi);
  const borderArgs0 = BORDER_WAVEFORM_CONTROL_ARGS.DATA_0;
  // What I've found is that 0x01 will have white boarder, 0x00 will have a black border.
  // No real idea what the rest of the values do. Just setting these based on what I find in the example code.
  const borderSetting = borderArgs0.SELECT_VBD.GS_TRANSITION | borderArgs0.GS_TRANSITION_CONTROL.FOLLOW_LUT | borderArgs0.VBD_TRANSITION.LUT1;
  sendData(borderSetting, connection.dcPin, connection.csPin, spi);
};
