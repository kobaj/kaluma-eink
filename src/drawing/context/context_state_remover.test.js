/* eslint-disable no-underscore-dangle */
const chai = require('chai');

const sinonChai = require('sinon-chai');
const { apply } = require('./context_state_remover.js');
const { stubGraphicsContext } = require('../../../fixtures/kaluma.js');

chai.use(sinonChai);

const { expect } = chai;

describe('context_state_remover', () => {
  describe('apply', () => {
    it('should work fine applying statelessness to regular context', () => {
      expect(apply(stubGraphicsContext())).to.not.be.null;
    });

    it('should work fine applying statelessness to the same context', () => {
      expect(apply(apply(stubGraphicsContext()))).to.not.be.null;
    });

    it('should work fine applying statelessness to empty context', () => {
      expect(apply({})).to.not.be.null;
    });

    it('should properly make drawRect stateless', () => {
      const context = apply(stubGraphicsContext());

      context.drawRect('a', 'b', { color: 1234 });

      expect(context.setColor).to.have.been.calledWith(1234);
      expect(context._drawRect).to.have.been.calledWith('a', 'b', { color: 1234 });
    });

    it('should handle non-stateless execution', () => {
      const context = apply(stubGraphicsContext());

      context.drawRect('a', 'b');

      expect(context._drawRect).to.have.been.calledWith('a', 'b');
    });
  });
});
