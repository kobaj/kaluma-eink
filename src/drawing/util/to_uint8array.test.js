const chai = require('chai');

const { toUint8Array } = require('./to_uint8array.js');

const { expect } = chai;

describe('toUint8Array', () => {
  it('should handle a single byte', async () => {
    expect(toUint8Array(0xFF)).to.deep.equal(new Uint8Array([0xFF]));
  });

  it('should handle an array of bytes', async () => {
    expect(toUint8Array([0xFF, 0x00])).to.deep.equal(new Uint8Array([0xFF, 0x00]));
  });

  it('should handle an existing Uint8Array', async () => {
    expect(toUint8Array(new Uint8Array([0xFF, 0x00]))).to.deep.equal(new Uint8Array([0xFF, 0x00]));
  });

  it('should handle a string array', async () => {
    expect(toUint8Array('\x00\x10\x10\x10\x10\x10\x10\x10\x00\x10\x00\x00')).to.deep.equal(new Uint8Array([0x00, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x00, 0x10, 0x00, 0x00]));
  });
});
