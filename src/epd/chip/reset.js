const { writePin } = require('../accessors/gpio.js');
const { timeout } = require('../accessors/timeout.js');

const LONG_DELAY_MS = 200;
const SHORT_DELAY_MS = 10;

/**
 * Reset the display synchronously, this will block.
 *
 * However, this does not confirm the display is ready
 * to receive commands. You should use hwReset command instead
 * which will wait for the display to be no longer busy.
 *
 * @param {(number|Object)} resetPin
 */
module.exports.resetSync = (resetPin) => {
  writePin(resetPin, HIGH);
  delay(LONG_DELAY_MS);
  writePin(resetPin, LOW);
  delay(SHORT_DELAY_MS);
  writePin(resetPin, HIGH);
  delay(LONG_DELAY_MS);
};

/**
 * Async version of reset, see resetSync for documentation.
 */
module.exports.reset = async (resetPin) => {
  writePin(resetPin, HIGH);
  await timeout(LONG_DELAY_MS);
  writePin(resetPin, LOW);
  await timeout(SHORT_DELAY_MS);
  writePin(resetPin, HIGH);
  await timeout(LONG_DELAY_MS);
};
