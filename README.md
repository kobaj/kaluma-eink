# Kaluma-eink

A library for controlling e-ink and e-paper displays (mostly from Waveshare) with Kaluma and an RP2040.

![e-ink displays and an RP2040 showing the text "Hello World"](promo_pic.png)

# Install

```
npm install https://gitlab.com/kobaj/kaluma-eink
```

### Supported Displays

I've only tested a few displays so far, a [1in54b_v2](https://www.waveshare.com/wiki/1.54inch_e-Paper_Module_(B)_Manual) ([code](./src/epd_1in54b_v2.js)), and a [22in66](https://www.waveshare.com/wiki/2.66inch_e-Paper_Module_Manual) ([code](./src/epd_2in66.js)). Though in theory any small eink display should work. You can even try to define your own by referencing [epd_generic.js](./src/epd/epd_generic.js).

If you are successful in running a new display, please send a pull request and we can include it in this library!

# Wiring

E-Ink | Name | Pico Pin | Type
------|------|----------|-----
DIN   | MOSI | 19       | SPI
CLK   | SCLK | 18       | SPI
CS    | CE0  | 17       | SPI
DC    |      | 13       | OUTPUT
RST   |      | 12       | OUTPUT
BUSY  |      | 11       | INPUT

# Drawing to the display

In order to draw to the e-ink display, you'll need to pick between the following

|                            | [EpdBufferedDriver](/src/drawing/graphics/epd_buffered_driver.js) | [EpdRawDriver](/src/drawing/graphics/epd_buffered_driver.js) |
| -------------------------- | ----------------------------------------------------------------- | ------------------------------------------------------------ |
| Works with e-ink           | Yes                                                               | Yes                                                          |
| Async interface available  | Yes                                                               | Yes                                                          |
| Memory Usage               | Lots                                                              | None                                                         |
| Built on Kaluma Graphics   | Yes                                                               | No                                                           |
| My personal recommendation | This                                                              | Not This                                                     |

# Using the EpdBufferedDriver

You can read more about Kaluma graphics capabilities at https://kalumajs.org/docs/api/graphics

```javascript
const { GPIO } = require('gpio');
const { SPI } = require('spi');

const {
  spiOptions, epd1in54bv2, EpdBufferedDriver, invert,
} = require('kaluma-eink');
const leesans = require('simple-fonts/lee-sans.js');

const sillycat = require('./sillycat_200.js');

const connection = {
  csPin: new GPIO(17, OUTPUT),
  dcPin: new GPIO(13, OUTPUT),
  resetPin: new GPIO(12, OUTPUT),
  busyPin: new GPIO(11, INPUT),
};

const spi = new SPI(0, spiOptions);

// Begin setting up a Kaluma graphics driver.
const epdDriver = new EpdBufferedDriver();
epdDriver.setup(connection, spi, epd1in54bv2);
const gc = epdDriver.getContext();

const RED = gc.color16(255, 0, 0);
const BLACK = gc.color16(0, 0, 0);
const WHITE = gc.color16(255, 255, 255);

// Use the Kaluma graphics driver.
gc.drawBitmap(0, 0, {
  width: sillycat.blackCanvas.resolutionX,
  height: sillycat.blackCanvas.resolutionY,
  bpp: 1,
  data: invert(sillycat.blackCanvas.image), // Kaluma graphics needs the bitmap inverted.
}, { color: BLACK });

gc.drawBitmap(0, 0, {
  width: sillycat.redCanvas.resolutionX,
  height: sillycat.redCanvas.resolutionY,
  bpp: 1,
  data: invert(sillycat.redCanvas.image),
}, { color: RED });

// Draw text.
gc.setFillColor(WHITE);
gc.setFontColor(RED);
gc.setFont(leesans);
gc.setFontScale(2, 2);

const topText = 'you like e-ink';
const topTextSize = gc.measureText(topText);
const topTextLocation = { x: (gc.getWidth() - topTextSize.width) / 2, y: 0 };
gc.fillRect(topTextLocation.x, topTextLocation.y, topTextSize.width, topTextSize.height);
gc.drawText(topTextLocation.x, topTextLocation.y, topText);

const bottomText = 'don\'t you';
const bottomTextSize = gc.measureText(bottomText);
const bottomTextLocation = { x: (gc.getWidth() - bottomTextSize.width) / 2, y: gc.getHeight() - bottomTextSize.height };
gc.fillRect(bottomTextLocation.x, bottomTextLocation.y, bottomTextSize.width, bottomTextSize.height);
gc.drawText(bottomTextLocation.x, bottomTextLocation.y, bottomText);

// Update the actual e-ink display.
gc.display();

spi.close();
```

# Using the EpdRawDriver

```javascript
const { GPIO } = require('gpio');
const { SPI } = require('spi');

const { spiOptions, epd1in54bv2, EpdRawDriver } = require('kaluma-eink');

const connection = {
  csPin: new GPIO(17, OUTPUT),
  dcPin: new GPIO(13, OUTPUT),
  resetPin: new GPIO(12, OUTPUT),
  busyPin: new GPIO(11, INPUT),

};

const spi = new SPI(0, spiOptions);

const driver = new EpdRawDriver();
driver.setup(connection, spi, epd1in54bv2);

const widthInBytes = Math.ceil(epd1in54bv2.resolutionX / 8);
const filler = Array.from({ length: widthInBytes - 1 }, () => 0xFF);

const blackImage = [
  0b00000000, ...filler,
  0b10000000, ...filler,
  0b11000000, ...filler,
  0b11100000, ...filler,
  0b11110000, ...filler,
  0b11111000, ...filler,
  0b11111100, ...filler,
  0b11111110, ...filler,
  0b11111111, ...filler,
];

const data = {
  blackImage,
};

driver.displayData(data);

spi.close();
```

### Drawing and Data

One bit per pixel. A display that is 152 pixels by 296 pixels will have 44,992 bits, or 5,624 bytes. The `x` axis is always the smaller of the two dimensions (so 152 in this example). Displays are always oriented with pixel 0 being at the top left hand corner. The pixel index is incremented left to right, top to bottom. 

A `1` indicates a white pixel, and a `0` indicates a color pixel. For most e-ink displays the color is black.

### Red Images

If your display supports red color alongside black, then you can provide it two images. One `blackImage` and/or one `redImage` inside the data argument. 

```javascript
// Same setup code as the example at the top.

const blackImage = [
  0b00000000, ...filler,
  0b11111111, ...filler,
  0b11111111, ...filler,
];

const redImage = [
  0b11111111, ...filler,
  0b00000000, ...filler,
  0b11111111, ...filler,
];

// This will display three stripes across the x axis. 
// First a black stripe, next a red stripe, and finally a white stripe.
driver.displayData({blackImage, redImage});
```

# Extras

### SPI

You can specify `spiOptions` yourself if you want, but don't go faster than 2000000 baud.

```javascript
const spiOptions = {
  mode: SPI.MODE_0,
  baudrate: 2000000,
  bitorder: SPI.MSB,
};
```

### Async

Both drivers support Async operation. 

```
// Same setup code as the example at the top.

const main = async () => {

  // Note this is the async method.
  await bufferedDriver.displayAsync();
  await rawDriver.displayDataAsync({blackImage, redImage});

  spi.close();
};

main();
```

### Bitmap image conversion tool

This library comes with a helpful utility to convert bitmap (and png, jpg, webp, etc) to a format that all three drawing formats above accept. Check out [the convert documentation](/bin/convert.md) for details.