const { sendCommand, sendData } = require('../chip/transfer.js');

const DISPLAY_UPDATE_CONTROL_1 = 0x21;
const DISPLAY_UPDATE_CONTROL_2 = 0x22;

const DISPLAY_UPDATE_CONTROL_1_ARGS = {
  DATA_0: {
    RED_RAM: {
      NORMAL: 0b00000000,
      BYPASS: 0b01000000,
      INVERSE: 0b10000000,
    },
    BLACK_RAM: {
      NORMAL: 0b0000,
      BYPASS: 0b0100,
      INVERSE: 0b1000,
    },
  },
  DATA_1: {
    UNKNOWN: {
      POR: 0b00000000,
    },
  },
};

const DISPLAY_UPDATE_CONTROL_2_ARGS = {
  DATA_0: {
    SEQUENCE: {
      ENABLE_CLOCK_SIGNAL: 0x80,
      DISABLE_CLOCK_SIGNAL: 0x01,
      ENABLE_ANALOG: 0xC0,
      DISABLE_ANALOG: 0x03,
      LOAD_LUT_DISPLAY_MODE_1: 0x91,
      LOAD_LUT_DISPLAY_MODE_2: 0x99,
      LOAD_TEMP_DISPLAY_MODE_1: 0xB1,
      LOAD_TEMP_DISPLAY_MODE_2: 0xB9,
      DISABLE_OSC_DISPLAY_MODE_1: 0xC7,
      DISABLE_OSC_DISPLAY_MODE_2: 0xCF,
      EVERYTHING_DISPLAY_MODE_1: 0xF7,
      EVERYTHING_DISPLAY_MODE_2: 0xFF,
    },
  },
};

/**
 * Specifies what values in ram mean.
 *
 * By default this will make color a 0 bit and white a 1 bit.
 * By default this will make source output mode S0, whatever that means.
 * By default this will do a full display update.
 *
 * @param {Object} connection the pins used to talk to the display
 * @param {Object} spi the spi connection to the display
 */
module.exports.displayUpdateControl = (connection, spi) => {
  sendCommand(DISPLAY_UPDATE_CONTROL_2, connection.dcPin, connection.csPin, spi);
  sendData(DISPLAY_UPDATE_CONTROL_2_ARGS.DATA_0.SEQUENCE.EVERYTHING_DISPLAY_MODE_1, connection.dcPin, connection.csPin, spi);

  sendCommand(DISPLAY_UPDATE_CONTROL_1, connection.dcPin, connection.csPin, spi);
  const displayArg0 = DISPLAY_UPDATE_CONTROL_1_ARGS.DATA_0;
  const ramReadSetting = displayArg0.RED_RAM.INVERSE | displayArg0.BLACK_RAM.NORMAL;
  sendData(ramReadSetting, connection.dcPin, connection.csPin, spi);
  sendData(DISPLAY_UPDATE_CONTROL_1_ARGS.DATA_1.UNKNOWN.POR, connection.dcPin, connection.csPin, spi);
};
